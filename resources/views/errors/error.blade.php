@extends('layouts.default')
@section('title','Панель управления')
@section('content')
@include('layouts.pageHeader',['pageHeader'=>'Ошибка'])
    @if ($debug)
    <div class="alert alert-danger fade in block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {{$errMessage}}
    </div>
    <div class="alert alert-warning fade in block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {{$errTrace}}
    </div>
    @else
        <h2>При выполнении запроса произошла ошибка, свяжитесь с техподдержкой!</h2>
    @endif
@stop