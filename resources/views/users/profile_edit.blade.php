@extends('layouts.default')
@section('title', $user['name'] . '. Редактирование профиля')
@section('content')
    @include('layouts.pageHeader',['pageHeader'=>'Профиль','pageDescription'=> 'Редактирование профиля'])
    @include('layouts.breadcrumbs',['urls'=>[
        ['uri'=>'/','name'=>'Рабочий стол'],],'currentUrl'=>'Профиль'])
    @include('layouts.errors')
    <form method="post" enctype="multipart/form-data">
        <div class="form-group row">
            <div class="col-md-6">
                <label>Имя пользователя:</label>
                <input name="name" type="text" class="form-control" value="{{ $user['name'] }}">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                <label>Nickname:</label>
                <input name="nickname" type="text" class="form-control" value="{{ $user['nickname'] }}">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                <label>Роль в команде:</label>
                <input name="role_in_team" type="text" class="form-control" value="{{ $user['role_in_team'] }}">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                <label>Email:</label>
                <input name="email" type="text" class="form-control" value="{{ $user['email'] }}">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                <label>Оповещать о новом сообщении:</label>

                <select class="form-control" name="time_alert_message">
                    @foreach($alert_time as $val)
                        <option value="{{ $val['time'] }}" @if($user['time_alert_message'] == $val['time']) selected @endif>{{ $val['name'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                <label>Оповещать о новом комментарии:</label>

                <select class="form-control" name="time_alert_comment">
                    @foreach($alert_time as $val)
                        <option value="{{ $val['time'] }}" @if($user['time_alert_comment'] == $val['time']) selected @endif>{{ $val['name'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                <label>Загрузить аватар:</label>
                <input name="avatar" type="file" class="styled">

                <div>
                    @if($user['avatar'])
                        <img src="/avatars/{{ $user['avatar'] }}" class="thumbnail" width="250" style="margin-top: 10px"/>
                    @endif
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                <label>Новый пароль:</label>
                <input name="password" type="password" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <label>Подтвердите пароль:</label>
                <input name="confirm_password" type="password" class="form-control">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-12 text-right">
                <button class="btn btn-primary" type="submit">Изменить данные</button>
            </div>
        </div>

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
    </form>
@stop