<!-- Unboxed grid -->
<div class="row">
    @foreach($users as $v)
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="block">
                <div class="thumbnail thumbnail-rectangle">
                    <div class="thumb">
                    <a href="/user/profile/{{ $v['id'] }}" class="thumb-zoom lightbox" title="{{ $v['name'] }}">
                        <div class="pre-img">
                            <img src="{!! $v['avatar'] ? '/avatars/'. $v['avatar'] : '//placehold.it/300' !!}" />
                        </div>
                    </a>

                    <div class="caption text-center">
                        <h6>{{ $v['name'] }}
                            <small>@if($v['role_in_team']) {{ $v['role_in_team'] }} @else - @endif</small>
                        </h6>

                        <div class="icons-group fs16">
                            @if ($User->role == 'SA')
                                <a href="/user/delete/{{ $v['id'] }}" title="Удалить" class="tip">
                                    <span data-icon="&#xe2a1;"></span>
                                </a>

                                <a href="/user/profile_edit/{{ $v['id'] }}" title="Редактировать" class="tip">
                                    <span data-icon="&#xe16f;"></span>
                                </a>
                            @endif

                            <a href="/user/profile/{{ $v['id'] }}" title="Профиль" class="tip"><span data-icon="&#xe384;"></span></a>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
<!-- /unboxed grid -->

<div class="cont_pagination">{!! $pagination !!}</div>