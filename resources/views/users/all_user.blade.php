@extends('layouts.default')
@section('title','Панель управления')
@section('content')
    @include('layouts.pageHeader', ['pageHeader' => 'Пользователи', 'pageDescription' => 'Список пользователей'])
    @include('layouts.breadcrumbs', ['urls' => [['uri'=>'/','name'=>'Рабочий стол'],], 'currentUrl' => 'Пользователи'])
    @include('layouts.errors')

    @if ($User->role=='A')
         <div class="info-blocks">
             <div class="text-right">
                <a class="btn btn-primary" href="/userAdd"><span>Добавить пользователя</span></a>
            </div>
        </div>
    @endif

    @include('users.all_user_render')
@stop