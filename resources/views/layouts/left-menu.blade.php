<div class="sidebar">
    <div class="sidebar-content">

        <div class="user-menu dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?=!empty($User->avatar) ? '/avatars/'.$User->avatar : 'http://placehold.it/300'?>">
                <div class="user-info">
                    {{ $User->name }} <span>{{ $User->roleName }}</span>
                </div>
            </a>
            <div class="popup dropdown-menu dropdown-menu-right">
                <div class="thumbnail">
                    <div class="thumb">
                        <img src="<?=!empty($User->avatar)?'/avatars/'.$User->avatar:'http://placehold.it/300'?>">
                        <div class="thumb-options">
                            <span>
                                <a href="/user/profile" class="btn btn-icon btn-success"><i class="icon-pencil"></i></a>
                                <a href="#" class="btn btn-icon btn-success"><i class="icon-remove"></i></a>
                            </span>
                        </div>
                    </div>

                    <div class="caption text-center">
                        <h6>{{ $User->name }} <small>{{ $User->roleName }}</small></h6>
                    </div>
                </div>

                <ul class="list-group">
                    <li class="list-group-item">
                        <i class="icon-pencil3 text-muted"></i>
                        Проектов
                        <span class="label label-success">{{ $project_count }}</span>
                    </li>

                    <li class="list-group-item">
                        <i class="icon-people text-muted"></i>
                        Задач
                        <span class="label label-info">{{ count($my_tasks) }}</span>
                    </li>
                </ul>
            </div>
        </div>

        <ul class="navigation">
            <li class="{!! Request::segment(2)=='profile' ? 'active' : '' !!}">
                <a href="/user/profile/{{ $User->id }}"><span>Профиль</span> <i class="icon-user"></i></a>
            </li>

            <li class="<?=Request::segment(1)=='dashboard'?'active':''?>">
                <a href="/dashboard"><span>Рабочий стол</span> <i class="icon-screen2"></i></a>
            </li>

            @if ($User->role == 'M' || $User->role == 'A' || $User->role == 'SA')
            <li class="<?=Request::segment(1)=='projects'?'active':''?>">
                <a href="/projects"><span>Проекты</span> <i class="icon-numbered-list"></i></a>
            </li>
            @endif

            @if ($User->role == 'M' || $User->role == 'A' || $User->role == 'SA')
            <li class="<?=Request::segment(1)=='tasks'?'active':''?>">
                <a href="/tasks"><span>Задачи</span> <i class="icon-grid"></i></a>
            </li>
            @endif

            @if ($User->role == 'SA')
            <li class="<?=Request::segment(1) == 'users' ? 'active' : ''?>">
                <a href="/users"><span>Пользователи</span> <i class="icon-users"></i></a>
            </li>
            @endif

            <li class="<?=Request::segment(1)=='messages'?'active':''?>">
                <a href="/messages"><span>Сообщения</span> <i class="icon-bubbles5"></i></a>
            </li>
        </ul>
    </div>
</div>