<?php $error=session('error');?>
<?php $info=session('info');?>
<?php $dump=session('dump');?>
@if (!empty($error))
    <div class="alert alert-danger fade in block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {{$error}}
    </div>
@endif
@if (!empty($info))
    <div class="alert alert-info fade in block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {{$info}}
    </div>
@endif
@if (!empty($dump))
    <div class="alert alert-warning fade in block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {{$dump}}
    </div>
@endif