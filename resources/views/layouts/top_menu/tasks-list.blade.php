<li class="dropdown">
    <a data-toggle="dropdown" class="dropdown-toggle">
        <i class="icon-grid"></i>
        <span class="label label-danger" style="background-color: #D65C4F;">{{ $high_tasks }}</span>
        <span class="label label-default">{{ count($my_tasks) }}</span>
    </a>
    <div class="popup dropdown-menu dropdown-menu-right">
        <div class="popup-header">
            <a href="#" class="pull-left"><i class="icon-spinner7"></i></a>
            <span>Мои задачи</span>
            <a href="#" class="pull-right"><i class="icon-new-tab"></i></a>
        </div>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Название</th>
                    <th class="text-center">Приоритет</th>
                </tr>
            </thead>
            <tbody>
            @foreach($my_tasks as $v)
                <tr>
                    <td>
                        <span class="status {!! $v->read ? 'status-success' : 'status-danger' !!} item-before"></span>
                        <a href="/task/detailed/{{ $v->id }}">{{ $v->name }}</a>
                    </td>
                    <td class="text-center">
                         <span class="label label-{{ $task_priority[$v->priority]['css'] }}" style="display: block;">
                             @if($v->processes == 'inwork') <i class="icon-play2"></i> @endif
                             {{ $task_priority[$v->priority]['name'] }}
                         </span>
                    </td>
                </tr>
            @endforeach

            @if(!count($my_tasks))
                <tr>
                    <td colspan="2">
                        Нет задач
                    </td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
</li>