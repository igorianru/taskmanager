<li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown">
        <i class="icon-people"></i>
        <span class="label label-default">{{count($Activities)}}</span>
    </a>
    <div class="popup dropdown-menu dropdown-menu-right" style="min-width: 600px;">
        <div class="popup-header">
            <a href="#" class="pull-left"><i class="icon-spinner7"></i></a>
            <span>Активность</span>
            <a href="#" class="pull-right"><i class="icon-paragraph-justify"></i></a>
        </div>
        <ul class="activity">
            @if (count($Activities)==0) <li>Нет активности!</li> @endif
            @foreach($Activities as $Activity)
            <li>
                <i class="{{\App\Classes\Base::$activity_actions[$Activity->action]}}"></i>
                <div>
                    {!!$Activity->text!!}
                    <span>{{$Activity->created_at}}</span>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
</li>