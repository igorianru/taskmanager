<div class="breadcrumb-line">
    <ul class="breadcrumb">
        @if (!empty($urls))
            @foreach($urls as $url)
                <li><a href="{{$url['uri']}}">{{$url['name']}}</a></li>
            @endforeach
        @endif
        @if (!empty($currentUrl))
            <li class="active">{{$currentUrl}}</li>
        @endif
    </ul>

    <div class="visible-xs breadcrumb-toggle">
        <a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a>
    </div>

    <ul class="breadcrumb-buttons collapse">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-search3"></i> <span>Поиск</span> <b class="caret"></b></a>
            <div class="popup dropdown-menu dropdown-menu-right">
                <div class="popup-header">
                    <a href="#" class="pull-left"><i class="icon-paragraph-justify"></i></a>
                    <span>Быстрый поиск</span>
                    <a href="#" class="pull-right"><i class="icon-new-tab"></i></a>
                </div>
                <form action="#" class="breadcrumb-search">
                    <input type="text" placeholder="Введите слово..." name="search" class="form-control autocomplete">
                    <div class="row">
                        <div class="col-xs-6">
                            <label class="radio">
                                <input type="radio" name="search-option" class="styled" checked="checked">
                                Всё
                            </label>
                            <label class="radio">
                                <input type="radio" name="search-option" class="styled">
                                Проекты
                            </label>
                        </div>

                        <div class="col-xs-6">
                            <label class="radio">
                                <input type="radio" name="search-option" class="styled">
                                Задачи
                            </label>
                            <label class="radio">
                                <input type="radio" name="search-option" class="styled">
                                Обсуждения
                            </label>
                        </div>
                    </div>

                    <input type="submit" class="btn btn-block btn-success" value="Поиск">
                </form>
            </div>
        </li>
    </ul>
</div>