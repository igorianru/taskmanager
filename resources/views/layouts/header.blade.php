<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <title>@yield('title')</title>

    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/css/londinium-theme.css" rel="stylesheet" type="text/css">
    <link href="/css/styles.css" rel="stylesheet" type="text/css">
    <link href="/css/icons.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">

    <script src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

    @if (Auth::check())
        <script type="text/javascript" src="/js/plugins/charts/sparkline.min.js"></script>
        <script type="text/javascript" src="/js/plugins/forms/uniform.min.js"></script>
        <script type="text/javascript" src="/js/plugins/forms/select2.min.js"></script>
        <script type="text/javascript" src="/js/plugins/forms/inputmask.js"></script>
        <script type="text/javascript" src="/js/plugins/forms/autosize.js"></script>
        <script type="text/javascript" src="/js/plugins/forms/inputlimit.min.js"></script>
        <script type="text/javascript" src="/js/plugins/forms/listbox.js"></script>
        <script type="text/javascript" src="/js/plugins/forms/multiselect.js"></script>
        <script type="text/javascript" src="/js/plugins/forms/validate.min.js"></script>
        <script type="text/javascript" src="/js/plugins/forms/tags.min.js"></script>
        <script type="text/javascript" src="/js/plugins/forms/switch.min.js"></script>

        <script type="text/javascript" src="/js/plugins/forms/uploader/plupload.full.min.js"></script>
        <script type="text/javascript" src="/js/plugins/forms/uploader/plupload.queue.min.js"></script>

        <script type="text/javascript" src="/js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
        <script type="text/javascript" src="/js/plugins/forms/wysihtml5/toolbar.js"></script>

        <script type="text/javascript" src="/js/plugins/interface/daterangepicker.js"></script>
        <script type="text/javascript" src="/js/plugins/interface/fancybox.min.js"></script>
        <script type="text/javascript" src="/js/plugins/interface/moment.js"></script>
        <script type="text/javascript" src="/js/plugins/interface/jgrowl.min.js"></script>
        <script type="text/javascript" src="/js/plugins/interface/datatables.min.js"></script>
        <script type="text/javascript" src="/js/plugins/interface/colorpicker.js"></script>
        <script type="text/javascript" src="/js/plugins/interface/fullcalendar.min.js"></script>
        <script type="text/javascript" src="/js/plugins/interface/timepicker.min.js"></script>

        <script type="text/javascript" src="/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/js/application.js"></script>

        <script type="text/javascript" src="http://91.240.87.136:8013/primus/primus.js"></script>
        <script src="/js/messages/jquery.session.js"></script>
        <script type="text/javascript" src="/js/primus_conf.js"></script>
        <script type="application/javascript">
			primus.primusInit({idUser: '{{ $User->id }}'});
        </script>
        <script src="/js/messages/jsdeferred.jquery.js"></script>
        <script src="/js/messages/messages.js"></script>
        <script>
			$(document).ready(function(){
				message.initialize({
					csrf_token: '{{ csrf_token() }}',
					primus: primus,
					id_this_user: '{{ $User->id }}',
					avatar_user: '{{ $User->avatar }}',
					role_user: '{{ $User->role }}',
					name_user: '{{ $User->name }}',
					user_role: '{!! json_encode($user_role) !!}'
				});
				message.resizeWin()
			});
			$(window).resize(function(){
				message.resizeWin()
			});
        </script>
        <script src="/js/gui.js"></script>
        <script>
			$(document).ready(function(){
				$.guiS.initialize({
					csrf_token: '{{ csrf_token() }}',
					id_this_user: '{{ $User->id }}',
					avatar_user: '{{ $User->avatar }}',
					role_user: '{{ $User->role }}',
					name_user: '{{ $User->name }}',
					user_role: '{!! json_encode($user_role) !!}'
				});
			});
        </script>
        @yield('header')
    @endif
</head>
@if (Auth::check())
    <body>
    @include('layouts.top_menu.main')
    <div class="page-container">
        @include('layouts.left-menu')
        <div class="page-content">
            @else
                <body class="full-width page-condensed">
@endif