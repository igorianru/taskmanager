@extends('layouts.default')
@section('title','Панель управления')
@section('content')
    @include('layouts.pageHeader', ['pageHeader' => 'Рабочий стол',
    'pageDescription' => $User->name.' вы являетесь ' . $user_role_name . ' в системе, вы можете '. $user_role_text
    ])

    @include('layouts.breadcrumbs',['urls'=>[['uri'=>'/','name'=>'Рабочий стол']]])
    @include('layouts.errors')
    <h6 class="heading-hr"><i class="icon-numbered-list"></i> Проекты</h6>
    @include('project.all_project')
    <br class="clear">

    <!-- Tasks table -->
    <div class="block">
        <h6 class="heading-hr"><i class="icon-grid"></i> Задачи</h6>
        <div class="datatable-tasks">
            @include('task.all_task_list', ['tasks' => $tasks_open])
        </div>
    </div>
    <!-- /tasks table -->

    <!-- Recent activity -->
    <div class="block">
        <h6 class="heading-hr"><i class="icon-file"></i> Активность</h6>
        <ul class="media-list">
            @foreach($activities as $v)
                <li class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object"
                             src="<?=!empty($v['avatar']) ? '/avatars/'. $v['avatar'] : 'http://placehold.it/300'?>"
                             alt=""
                        >
                    </a>

                    <div class="media-body">
                        <div class="clearfix">
                            <span class="media-notice">{{ $v['created_at'] }}</span>
                        </div>
                    </div>

                    {!! $v['text'] !!}
                </li>
            @endforeach

            <li class="text-center">{!! $pagination !!}</li>
        </ul>
    </div>
    <!-- /recent activity -->
@stop