<div>
    @if(isset($body['text']))

    <div style="padding: 2px; background: #f7fcfe">
        <div style="width: 120px;min-height: 100px; float: left; padding: 5px">
            <img width="120" src="{{ $body['url'] }}<?=!empty($body['user_from']->avatar)?'/avatars/'.$body['user_from']->avatar:'http://placehold.it/300'?>">
        </div>
        <div style="width: 500px; float: left; padding: 5px">
            <div>{{ $body['user_from']->name }}, оставил Вам новое сообщение</div>
            <div>{{ $body['text'] }}</div>
        </div>
        <div style="clear: both; width: 100%;"></div>
    </div>
    @endif

    <br />
    @if($body['alert'])
    <p>
        У вас {{ $body['alert'] + 1 }} непрочитанных сообщений.
    </p>
    @endif

    Для прочтения войдите в систему <a href="{{ $body['url'] }}">TaskManager</a>
</div>
