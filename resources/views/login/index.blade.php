@extends('layouts.default')
@section('title','Авторизация')
@section('content')
@include('layouts.errors')
    <div class="login-wrapper">
        <form action="/login" method="post" role="form">
            <div class="popup-header">
                <span class="text-semibold">Авторизация</span>
            </div>
            <div class="well">
                <div class="form-group has-feedback">
                    <label>Логин</label>
                    <input name="login" type="text" class="form-control" placeholder="Логин">
                    <i class="icon-users form-control-feedback"></i>
                </div>

                <div class="form-group has-feedback">
                    <label>Пароль</label>
                    <input name="password" type="password" class="form-control" placeholder="Пароль">
                    <i class="icon-lock form-control-feedback"></i>
                </div>

                @if (!empty($error))
                <div class="alert alert-danger fade in block-inner">
                    <span>{{{$error}}}</span>
                </div>
                @endif

                <div class="row form-actions">
                    <div class="col-xs-6">
                        <div class="checkbox checkbox-success">
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <button type="submit" class="btn btn-warning pull-right"><i class="icon-menu2"></i> Войти</button>
                    </div>
                </div>
            </div>
            <input type="hidden" name="_token" value="<?=csrf_token()?>"/>
        </form>
    </div>
@stop