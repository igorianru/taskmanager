@extends('layouts.default')
@section('title','Панель управления')
@section('content')
@include('layouts.pageHeader',['pageHeader' => $project->name,'pageDescription'=>'Редактирование проекта'])
@include('layouts.breadcrumbs',['urls'=>[['uri'=>'/projects','name' => 'Проекты ']]])
@include('layouts.errors')
    <form method="post" action="/project/edit/{{ $project->id }}">
        <div class="form-group row">
            <div class="col-md-6">
                <label>Название проекта:</label>
                <input name="name" value="{{ $project->name  }}" type="text" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12s">
                <label>Описание проекта:</label>
                <textarea  style="min-height: 250px" name="description" class="form-control editor">{{ $project->description  }}</textarea>
            </div>
        </div>
        <h6 class="heading-hr">Добавить пользоваетлей в проект</h6>
        <div id="assignedUsers">
            <span class="label label-block label-info text-left">Добавленные пользователи:</span>
            <br />




            <div id="assignedUser_" class="form-group row assigned">
                <div class="col-md-4 user_con" style="display: none">
                    <label>Пользователь</label>
                    <select class="user_select form-control">
                        @foreach($allUsers as $user)
                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4 role_con" style="display: none">
                    <label>Роль</label>
                    <select class="role_select form-control">
                        <option value="1">Администратор</option>
                        <option value="2">Менеджер</option>
                        <option value="3">Пользователь</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <button data-id="0" type="button" class="add-user-btn btn btn-success btn-icon" style="margin-top: 21px">
                        <i class="icon-plus"></i>
                    </button>
                    <button data-id="0" type="button" class="delete-user-btn btn btn-danger btn-icon" style="margin-top: 21px;display: none">
                        <i class="icon-minus"></i>
                    </button>
                </div>
            </div>
            <?php $i = 0;?>
            @foreach($user_list as $val)
                <div id="assignedUser_{{ $val->id }}" class="form-group row assigned">
                    <div class="col-md-4">
                        <label>Пользователь</label>
                        @foreach($allUsers as $v)
                            @if($val->userId == $v->id)
                                <input type="text" class="form-control" value="{{ $v->name }}" disabled>
                                <input type="hidden" name="user[]" value="{{ $v->id }}">
                            @endif
                        @endforeach
                    </div>
                    <div class="col-md-4">
                        <label>Роль</label>

                        <select name="role[]" class="form-control">
                            <option value="A" @if($val->role == 'A') selected @endif>Администратор</option>
                            <option value="M" @if($val->role == 'M') selected @endif>Менеджер</option>
                            <option value="U" @if($val->role == 'U') selected @endif>Пользователь</option>
                        </select>
                    </div>

                    <div class="col-md-4">
                        <button data-id="{{ $val->id }}" type="button" class="delete-user-btn btn btn-danger btn-icon" style="margin-top: 21px; display: inline-block;">
                            <i class="icon-minus"></i>
                        </button>
                    </div>
                </div>
                <?php $i++; ?>
            @endforeach
        </div>
        <div class="form-group row">
            <div class="col-md-12 text-right">
                <button class="btn btn-primary" type="submit">Сохранить</button>
            </div>
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
    </form>
<script>
    var countAssignedUsers = '{{ count($user_list) }}';
    $(document).ready(function () {
        $('#assignedUsers').on('click', '.add-user-btn', function () {
            countAssignedUsers = countAssignedUsers + 1;
            var assignedContent = $('#assignedUser_').clone();
            var addBtn = assignedContent.find('.add-user-btn').hide();
            var deleteBtn = assignedContent.find('.delete-user-btn');

            assignedContent.find('.role_select').attr('name', 'role[]');
            assignedContent.find('.user_select').attr('name', 'user[]');
            assignedContent.find('.role_con').show();
            assignedContent.find('.user_con').show();
            assignedContent.find('.delete-user-btn').attr('data-id', countAssignedUsers);

            addBtn.data('id', countAssignedUsers);
            deleteBtn.data('id', countAssignedUsers);
            assignedContent.attr('id', 'assignedUser_' + countAssignedUsers);
            deleteBtn.show();
            $('#assignedUsers').append(assignedContent);
        });

        $('#assignedUsers').on('click','.delete-user-btn',function(){
            var id = $(this).attr('data-id');
            $('#assignedUser_' + id).remove();
        });
    });
</script>
@stop