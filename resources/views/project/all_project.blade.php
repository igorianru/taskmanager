@if (count($projects) == 0) <h3>У вас нет проектов</h3> @endif

@foreach($projects as $val)
    <div class="col-md-12 id-p-{{ $val->id }}">
        <div class="block task @if($val->status == 'closed') task-high @else task-inwork @endif">
            <div class="row with-padding">
                <div class="col-sm-9">
                    <div class="task-description">
                        <a href="/project/{{ $val->id }}">{{ $val->name }}</a>
                        <span>{{ $val->description }}</span>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="task-info">
                        <span>{{$val->created_at}}</span>

                        <span>
                            <?php
                            $now = new \DateTime();
                            $old = new \DateTime($val->created_at);
                            $diff = $now->diff($old);
                            echo \App\Classes\Base::plural($diff->days,['день','дня','дней']);
                            ?> | <span class="label label-danger">0%</span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="pull-left">
                    <span><i class="icon-clock"></i></span>
                </div>

                <div class="pull-right">
                    <ul class="footer-icons-group">
                        @if ($User->role == 'A' || $User->role == 'M' || $User->role == 'SA')
                            <li>
                                <a href="/project/edit/{{ $val->id }}">
                                    <i class="icon-pencil"></i>
                                </a>
                            </li>

                            <li>
                                <a class="removeProject" onclick="$.guiS.deleteProject({{ $val->id }})"
                                   data-id="{{ $val->id }}">
                                    <i class="icon-remove3"></i>
                                </a>
                            </li>

                            <li class="project-status-li">
                                @if($val->status == 'closed')
                                    <a title="Возобновить проект" onclick="$.guiS.statusProject({{ $val->id }}, 'active')">
                                        <i class="icon-play3"></i>
                                    </a>
                                @else
                                    <a title="Завершить проект"  onclick="$.guiS.statusProject({{ $val->id }}, 'closed')">
                                        <i class="icon-stop2"></i>
                                    </a>
                                @endif
                            </li>
                        @endif

                        <li class="project-status-li-text">
                            @if($val->status == 'closed')
                                <a title="Проект закрыт">
                                    Проект закрыт
                                </a>
                            @else
                                <a title="Активный проект">
                                    Активный проект
                                </a>
                            @endif
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endforeach