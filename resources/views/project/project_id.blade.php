@extends('layouts.default')
@section('title','Панель управления')
@section('content')

    @include('layouts.pageHeader', ['pageHeader' => $project->name,
    'pageDescription' => $project->description
    ])
    @include('layouts.breadcrumbs',['urls'=>
    [['uri'=>'/','name'=>'Рабочий стол'],['uri'=>'/projects','name'=>'Проекты']],'currentUrl'=> $project->name])
    @include('layouts.errors')

    <!-- Profile grid -->
    <div class="row">
        <div class="col-lg-10">

            <!-- Page tabs -->
            <div class="tabbable page-tabs">
                <ul class="nav nav-justified nav-tabs">
                    <li class="active">
                        <a href="#activity" data-toggle="tab"><i class="icon-paragraph-justify2"></i> Активность</a>
                    </li>
                    <li>
                        <a href="#tasks" data-toggle="tab">
                            <i class="icon-settings"></i>
                            Задачи
                            <span class="label label-success">{{ count($tasks_open) }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="#stats" data-toggle="tab">
                            <i class="icon-stats2"></i>
                            Статистика
                            <i class="icon-spinner7 spin pull-right"></i>
                        </a>
                    </li>
                    <li><a href="#users" data-toggle="tab"><i class="icon-users"></i>Пользователи</a></li>
                </ul>
                <div class="tab-content">

                    <!-- First tab -->
                    <div class="tab-pane active fade in" id="activity">

                        <!-- Statistics -->
                        <div class="block">
                            <div class="panel panel-primary">
                                <div class="panel-heading collapsed pointer" data-toggle="collapse" href="#descriptionProject">
                                    <h6 class="panel-title">
                                        Описание проекта {{  $project->name }}
                                    </h6>
                                </div>
                                <div id="descriptionProject" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body">
                                        {{ $project->description }}
                                    </div>
                                </div>
                            </div>


                            <ul class="statistics list-justified">
                                <li>
                                    <div class="statistics-info">
                                        <a href="#" title="" class="bg-primary"><i class="icon-people"></i></a>
                                        <strong>{{ $users_count }}</strong>
                                    </div>
                                    <span>Пользователей в проекте</span>
                                </li>
                                <li>
                                    <div class="statistics-info">
                                        <a href="#" title="" class="bg-success"><i class="icon-list"></i></a>
                                        <strong>{{ count($tasks_open) }}</strong>
                                    </div>
                                    <span>Открытых задач</span>
                                </li>
                                <li>
                                    <div class="statistics-info">
                                        <a href="#" title="" class="bg-inwork"><i class="icon-play3"></i></a>
                                        <strong>{{ count($tasks_active) }}</strong>
                                    </div>
                                    <span>Задачи в исполении</span>
                                </li>
                                <li>
                                    <div class="statistics-info">
                                        <a href="#" title="" class="bg-close"><i class="icon-checkmark3"></i></a>
                                        <strong>{{ count($tasks_closed) }}</strong>
                                    </div>
                                    <span>Закрытых задач</span>
                                </li>
                            </ul>
                        </div>
                        <!-- /statistics -->

                        <!-- Search line -->
                        <form action="#" class="search-line block" role="form">
                            <span class="subtitle"><i class="icon-pencil3"></i> Поиск по активности:</span>
                            <div class="input-group">
                                <div class="search-control">
                                    <input type="text" class="form-control autocomplete" placeholder="Что выхотите найти?">
                                    <select multiple="multiple" class="multi-select-search" tabindex="2">
                                        <option value="Users">Задачи</option>
                                        <option value="Profiles">Пользователи</option>
                                        <option value="Images">Комментарии</option>
                                        <option value="Connections">Сообщения</option>
                                    </select>
                                </div>
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="button">Искать</button>
                                </span>
                            </div>
                        </form>
                        <!-- /search line -->
                    </div>

                    <div class="tab-pane fade" id="tasks">
                        <div class="tabbable page-tabs">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#all-tasks" data-toggle="tab">
                                        <i class="icon-paragraph-justify2"></i>
                                        Открытые
                                        <span class="label label-info">{{ count($tasks_open) }}</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="#active" data-toggle="tab">
                                        <i class="icon-play2"></i>
                                        В исполении
                                        <span class="label label-info">{{ count($tasks_active) }}</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="#closed" data-toggle="tab">
                                        <i class="icon-checkmark3"></i>
                                        Закрытые
                                        <span class="label label-info">{{ count($tasks_closed) }}</span>
                                    </a>
                                </li>

                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-cogs"></i> Управление <b class="caret"></b>
                                    </a>

                                    <ul class="dropdown-menu">
                                        @if ($User->role == 'A' || $User->role == 'M' || $User->role == 'SA')
                                            <li>
                                                <a href="/task/{{ $project->id }}">
                                                    <i class="icon-plus"></i> Добавить задачу
                                                </a>
                                            </li>
                                        @endif

                                        @if ($User->role == 'A' || $User->role == 'SA')
                                            <li>
                                                <a href="/project/edit/{{ $project->id }}" title="Редактировать проект">
                                                    <i class="icon-pencil"></i> Редактировать
                                                </a>
                                            </li>
                                        @endif
                                    </ul>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane active fade in" id="all-tasks">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h6 class="panel-title"><i class="icon-paragraph-justify2"></i> Задачи</h6>
                                        </div>
                                        <div class="datatable-tasks">
                                            @include('task.all_task_list', ['tasks' => $tasks_open])
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="active">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h6 class="panel-title"><i class="icon-paragraph-justify2"></i> Задачи</h6>
                                        </div>
                                        <div class="datatable-tasks">
                                            @include('task.all_task_list', ['tasks' => $tasks_active])
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="closed">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h6 class="panel-title"><i class="icon-paragraph-justify2"></i> Задачи</h6>
                                        </div>
                                        <div class="datatable-tasks">
                                            @include('task.all_task_list', ['tasks' => $tasks_closed])
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="stats">stats</div>
                    <div class="tab-pane fade" id="users">@include('users.all_user_render')</div>
                </div>
            </div>
        </div>

        <div class="col-lg-2">
            <div class="block">

                <div class="block">
                    <div class="thumbnail">
                        <div class="thumb">
                            <img alt="" src="http://placehold.it/300">
                        </div>

                        <div class="caption text-center">
                            <h6>{{ $project->name }}</h6>
                            <div class="icons-group">
                                <a href="#" title="Google Drive" class="tip"><i class="icon-google-drive"></i></a>
                                <a href="#" title="Twitter" class="tip"><i class="icon-twitter"></i></a>
                                <a href="#" title="Github" class="tip"><i class="icon-github3"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <ul class="nav nav-list">
                    <li class="nav-header">Настройки <i class="icon-cogs"></i></li>
                    <li><a href="#">Редактировать проект</a></li>
                    <li><a href="#">Изменить фото</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#" style="color: #e52a29">Остановить проект</a></li>
                </ul>
            </div>
        </div>
    </div>
@stop