@extends('layouts.default')
@section('title','Панель управления')
@section('content')
    @include('layouts.pageHeader',['pageHeader' => 'Проекты', 'pageDescription' => 'Все проекты'])
    @include('layouts.breadcrumbs',['urls' => [['uri'=>'/','name'=>'Рабочий стол'],['uri' => '/projects', 'name' => 'Проекты']]])
    @include('layouts.errors')

    @if ($User->role == 'A' || $User->role == 'SA')
        <div class="info-blocks">
            <div class="text-right">
                <a class="btn btn-primary" href="projects/add">Создать проект</a>
            </div>
        </div>
    @endif

    {{-- all_project --}}
    @include('project.all_project')
@stop