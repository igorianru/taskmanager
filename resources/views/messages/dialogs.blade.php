@extends('layouts.default')
@section('title','Сообщения: все диалоги')
@section('content')
    @include('layouts.pageHeader',['pageHeader'=>'Сообщения','pageDescription'=>''])
    @include('layouts.errors')

    <div class="tabbable page-tabs">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#last_dialog" onclick="message.showDialog(0);" class="last_dialog" data-toggle="tab">
                    <i class="icon-bubble3"></i>
                    Последние диалоги
                </a>
            </li>

            <li class="">
                <a href="#new_dialog" data-toggle="tab">
                    <i class="icon-bubble-user"></i>
                    Новый диалог
                </a>
            </li>

            <li class="" id="see_dialog_li" style="display: none">
                <a href="#see_dialog" data-toggle="tab">
                    <i class="icon-bubble-dots2"></i>
                    Просмотр диалогов
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <!--last_dialog-->
            <div class="tab-pane fade active in" id="last_dialog">
                <ul class="message-list dialog-last">
                </ul>
            </div>
            <!--end last_dialog-->

            <!--new_dialog-->
            <div class="tab-pane fade in" id="new_dialog">
                <ul class="message-list dialog-new">
                    @foreach($all_users as $user)
                        <script type="application/javascript">
							$(window).ready(function() {
								primus.checkUserOnline({{$user->id}});
							})
                        </script>
                        <li
                            class="newDialogUs dialogUs-{{$user->id}}"
                            title="Начать диалог с {{$user->name}}"
                            data-user='{"id":"{{$user->id}}", "name":"{{ $user->name }}"}'
                        >
                            <div class="clearfix">
                                <div class="chat-member">
                                    @if($user['avatar'])
                                        <a href="#"><img src="{{'/avatars/'. $user['avatar']}}" alt=""></a>
                                    @else
                                        <a href="#"><img src="{{'http://placehold.it/300'}}" alt=""></a>
                                    @endif
                                    <h6>
                                        {{$user->name}}
                                        <span class="status status-success"></span>
                                        <small>&nbsp; /&nbsp; {{ $user_role[$user->role] }}</small>
                                    </h6>
                                </div>

                                <div class="chat-actions">
                                    <a
                                        class="btn btn-link btn-icon btn-xs collapsed"
                                        data-toggle="collapse"
                                        href="javascript:void(0)"
                                    ><i class="icon-bubble3"></i></a>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
            <!--end new_dialog-->

            <!--see_dialog-->
            <div class="tab-pane fade in" id="see_dialog">
                <div class="tabbable">
                    <ul class="nav nav-pills nav-pills" id="navDialTop"></ul>
                    <div class="tab-content pill-content" id="navDialBottom"></div>
                </div>
            </div>
            <!--end see_dialog-->
        </div>
    </div>
@stop