<table class="table table-bordered">
    <thead>
    <tr>
        <th class="task-sharp">#</th>
        <th>Описание задачи</th>
        <th class="task-priority">Приоритет</th>
        <th class="task-status">Статус</th>
        <th class="task-date-added">Дата добавления</th>
        <th class="task-progress">Прогерсс</th>
        <th class="task-deadline">Дедлайн</th>
        <th class="task-user">Отвественный</th>

        @if(isset($tasks[0]->projects_name))
            <th class="task-project">Проект</th>
        @endif

        <th class="task-tools text-center">Действия</th>
    </tr>
    </thead>

    <tbody>
    @foreach($tasks as $v)
        <tr class="id-t-{{ $v->id }}">
            <td class="task-desc">
                #{{ $v->number_task }}
            </td>

            <td class="task-desc">
                <a href="/task/detailed/{{ $v->id }}">{{ $v->name }}</a>
                <span>{{ mb_substr(strip_tags($v->description), 0, 70) . '...' }}</span>
            </td>

            <td class="text-center">
                <p>
                    <span class="label label-{{\App\Classes\Base::$task_priority[$v->priority]['css']}}" style="display: block">
                        {{\App\Classes\Base::$task_priority[$v->priority]['name']}}
                    </span>
                </p>
            </td>

            <td class="text-center">
                @if($v->status != 'reading')
                    <p>
                        <span class="label label-{{\App\Classes\Base::$task_status[$v->status]['css']}}" style="display: block">
                            {{\App\Classes\Base::$task_status[$v->status]['name']}}
                        </span>
                    </p>
                @endif

                <p class="label-status">
                    <span class="label label-{{ $statuses_color[$v->processes] }}" style="display: block">
                        {{ $statuses_text[$v->processes] }}
                    </span>
                </p>
            </td>

            <td>{{ $v->created_at }}</td>
            <td>
                @if($v->tasks == '' || $v->tasks == '[]')
                    Однозадачная
                @else
                    <?php
                    $tasks = json_decode($v->tasks, true);
                    $count_checked = 0;

                    for($i = 0; count($tasks) > $i; $i++) { if($tasks[$i]['status'] == 'checked') ++$count_checked; }
                    $percent = round(100 / count($tasks) * $count_checked, 0);
                    ?>

                    <div class="progress progress-micro">
                        <div
                            class="progress-bar progress-bar-info"
                            role="progressbar"
                            aria-valuenow="{{ $v->progress }}"
                            aria-valuemin="0"
                            aria-valuemax="100"
                            style="width: {{ $percent }}%;"
                        ></div>
                    </div>

                    <span class="">{{ $percent }}% выполнено</span>
                @endif
            </td>

            <td>
                <i class="icon-clock"></i>
                <strong class="text-danger">
                    {{ $v->deadline ? $v->deadline : 'deadline не задан' }}
                </strong>
                <!--<i class="icon-clock"></i> <strong class="text-danger">14</strong> hours left-->
            </td>

            <td>
                {{ $v->user_name }}
            </td>

            @if($v->projects_name != '')
                <td>
                    <a href="/project/{{ $v->projectId }}">{{ $v->projects_name }}</a>
                </td>
            @endif

            <td class="text-center">
                <div class="btn-group">
                    <button type="button" class="btn btn-icon btn-success dropdown-toggle" data-toggle="dropdown"><i class="icon-cog4"></i></button>
                    <ul class="dropdown-menu icons-right dropdown-menu-right">
                        <li>
                            <a href="/task/edit/{{ $v->id }}">
                                <i class="icon-quill2"></i> Редактировать
                            </a>
                        </li>

                        @if($v->status != 'closed')
                            <li class="close-task-li">
                                <a onclick="$.guiS.closeTask({{ $v->id }})" class="close-task">
                                    <i class="icon-checkmark3"></i>
                                    Закрыть
                                </a>
                            </li>
                        @endif

                        @if ($User->role == 'A' || $User->role == 'SA')
                            <li>
                                <a onclick="$.guiS.deleteTask({{ $v->id }})" class="delete-task">
                                    <i class="icon-remove2"></i>
                                    Удалить
                                </a>
                            </li>
                        @endif
                    </ul>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>