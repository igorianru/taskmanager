@extends('layouts.default')
@section('title','Панель управления')
@section('content')
    @include('layouts.pageHeader',['pageHeader' => 'Задача', 'pageDescription' => 'Детальный просмотр задачи'])
    @include('layouts.breadcrumbs',['urls' => [
        ['uri' => '/', 'name' => 'Рабочий стол'],
        ['uri' => '/project/' . $project->id, 'name' => $project->name]],
        'currentUrl' =>'#' . $task->number_task . ' '. $task->name])

    @include('layouts.errors')

    <script src="/js/lodash.min.js"></script>
    <script src="/js/fileM.js"></script>
    <script>
		$(document).ready(function(){
			$.fileM.initialize({
				attach      : '{{ $task->attachment }}',
				csrf_token  : '{{ csrf_token() }}',
				id_this_user: '{{ $User->id }}',
				role_user   : '{{ $User->role }}',
				name_user   : '{{ $User->name }}',
				id_project  : '{{ $task->projectId }}',
				id_task     : '{{ $task->id }}',
				user_role   : '{!! json_encode($user_role) !!}',
				type_mini   : true
			});
		});
    </script>

    <div class="row id-p-{{ $task->id }}">
        <div class="col-lg-8">
            <!-- Task description -->
            <div class="block">
                <h5>{{ '#' . $task->number_task . ' '. $task->name }}</h5>
                <ul class="headline-info">
                    <li>Проект: <a href="/project/{{ $project->id }}">{{ $project->name }}</a></li>
                    <li>
                        Статус чтения:
                        <span class="text-semibold text-{{ $task_status[$task->status]['css'] ?? '' }}">
                            {{ $task_status[$task->status]['name'] }}
                        </span>
                    </li>

                    <li>
                        Статус выполнения:
                        <span class="text-semibold text-{{ $task_status[$task->processes]['css'] }}">
                            {{ $statuses_text[$task->processes] }}
                        </span>
                    </li>

                    <li>
                        Создал:
                        <a href="/user/profile/{!! $user_create->id ?? '' !!}">{!! $user_create->name ?? '' !!}</a>
                    </li>

                    <li>
                        Исполнитель:
                        <a href="/user/profile/{!! $user_assigned->id ?? '' !!}">{!! $user_assigned->name ?? '' !!}</a>
                    </li>
                </ul>

                <hr>

                <div class="block-inner">
                    <p>{!! $task->description !!}</p>
                </div>

                <div class="block-inner">
                    <?Php
                    $tasks_in_task = count(json_decode($task->tasks, true)) > 0
                        ? json_decode($task->tasks, true)
                        : []
                    ?>


                    @foreach($tasks_in_task as $k => $q)
                        <div style="margin-bottom: 15px; border: solid 1px #eeeeee; padding: 10px 5px">
                             <span class="checked">
                                 <input
                                     autocomplete="off"
                                     onclick="$.guiS.checkedTask('{{ $task->id }}',
                                         '<?php echo $q['status'] !== 'checked' ? 'checked' : '' ?>',
                                         '{{ $k }}')"

                                     type="checkbox"
                                     <?php echo $q['status'] === 'checked' ? 'checked' : '' ?>
                                     class="styled"
                                 />
                             </span>

                            @if($q['status'] == 'checked')
                                <span class="checked-{{ $k }}" style="text-decoration: line-through">
                                    {{ $q['text'] }}
                                </span>
                            @else
                                <span class="checked-{{ $k }}">{{ $q['text'] }}</span>
                            @endif
                        </div>
                    @endforeach
                </div>

                <div class="panel-footer">
                    <div class="pull-left">
                        <ul class="footer-links-group">
                            <li>
                                <i class="icon-plus-circle muted"></i>
                                Добавлена:
                                <a href="#" class="text-semibold">{{ $task->created_at }}
                                </a>
                            </li>

                            <li>
                                <i class="icon-checkmark-circle muted"></i>
                                Выполнить:
                                <a href="#" class="text-semibold">{!! $task->deadline ?? 'бессрочная' !!}</a>
                            </li>

                            <li class="has-label">
                                @if($task->tasks == '' || $task->tasks == '[]')
                                    Однозадачная
                                @else
                                    <?php
                                    $tasks = json_decode($task->tasks, true);
                                    $count_checked = 0;

                                    for($i = 0; count($tasks) > $i; $i++) { if($tasks[$i]['status'] == 'checked') ++$count_checked; }
                                    $percent = round(100 / count($tasks) * $count_checked, 0);
                                    ?>

                                    <div class="progress progress-micro">
                                        <div
                                            class="progress-bar progress-bar-info"
                                            role="progressbar"
                                            aria-valuenow="{{ $task->progress }}"
                                            aria-valuemin="0"
                                            aria-valuemax="100"
                                            style="width: {{ $percent }}%;"
                                        ></div>
                                    </div>

                                    <span class="">{{ $percent }}% выполнено</span>
                                @endif
                            </li>

                            <li>
                                <i class="icon-bubble5 muted"></i>
                                <a href="#" class="text-semibold">{{ count($comments) }}</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <h6 class="heading-hr"><i class="icon-bubble"></i> Комментарии</h6>
            <div class="block">
                @if (count($comments)==0) <p>Нет комментариев!</p> @endif
                @foreach($comments as $c)

                    <div class="media">
                        <a class="pull-left" href="#">
                            <img class="media-object" src="<?=!empty($users[$c->userId]->avatar)?'/avatars/'.$users[$c->userId]->avatar:'http://placehold.it/300'?>" alt="">
                        </a>
                        <div class="media-body">
                            <a href="#" class="media-heading">{{ $users[$c->userId]->name }}</a>

                            <ul class="headline-info">
                                <!--li><a href="#">Редактировать</a></li-->
                                <li>
                                    <a
                                        href="#deleteComment_modal"
                                        class="delete-comment"
                                        data-toggle="modal"
                                        data-id="{{ $c->id }}"
                                        data-taskId="{{ $task->id }}"
                                    >Удалить</a>
                                </li>
                            </ul>

                            {{ $c->text }}
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="block">
                <h6><i class="icon-bubble-plus"></i> Добавить комментарий</h6>
                <div class="well">
                    <form action="/task/{{ $task->id }}/addcomment" method="post" role="form">
                        <div class="form-group">
                            <label>Комментарий:</label>

                            <textarea
                                name="comment"
                                rows="5"
                                cols="5"
                                placeholder="Сообщение..."
                                class="elastic form-control"
                            ></textarea>
                        </div>

                        <div class="form-actions text-right">
                            <input type="submit" value="Добавить" class="btn btn-primary">
                        </div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h6 class="panel-title"><i class="icon-settings"></i> Параметры задачи</h6>
                </div>

                <div class="panel-body"><ul class="timer">
                        <li class="project-status-li-text">{{ $statuses_text[$task->processes] }}</li>
                    </ul>
                </div>

                <div class="panel-footer">
                    <div class="pull-left">
                        <ul class="footer-icons-group">
                            <li>
                                <a href="/task/edit/{{ $task->id }}" title="Редактировать">
                                    <span class="icon-quill2"></span>
                                </a>
                            </li>

                            @if ($User->role == 'A' || $User->role == 'SA')
                                <li>
                                    <a onclick="$.guiS.deleteTask({{ $task->id }}, {{ $task->projectId }})" class="delete-task" title="Удалить">
                                        <span class="icon-remove2"></span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div>

                    <div class="pull-right">
                        <ul class="project-BB footer-icons-group">
                            <li class="project-paused-li project-inwork-li">
                                <?
                                $rpss = array_search($task->processes, $statuses['closed']);
                                $rpssT = ($rpss === false) ? 'paused' : 'inwork';
                                ?>
                                <a
                                    onclick="$.guiS.processesTask('{{ $task->id }}', '{{ $rpssT }}')"
                                    href="#" class="@if($task->processes == 'inwork') activeBB @endif"
                                >
                                    @if($rpss === false)
                                        <i class="icon-pause"></i>
                                    @else
                                        <i class="icon-play2"></i>
                                    @endif

                                     <span>In work</span>
                                </a>
                            </li>

                            <li class="project-review-li">
                                <a
                                    onclick="$.guiS.processesTask('{{ $task->id }}', 'review')"
                                    href="#" class="@if($task->processes == 'review') activeBB @endif"
                                >
                                    <i class="icon-tv"></i>
                                    <span>Review</span>
                                </a>
                            </li>

                            <li class="project-test-li">
                                <a
                                    onclick="$.guiS.processesTask('{{ $task->id }}', 'test')"
                                    class="@if($task->processes == 'test') activeBB @endif"
                                >
                                    <i class="icon-spinner7"></i>
                                    <span>Test</span>
                                </a>
                            </li>

                            <li class="project-closed-li">
                                <a
                                    onclick="$.guiS.processesTask('{{ $task->id }}', 'closed')"
                                    href="#" class="@if($task->processes == 'closed') activeBB @endif"
                                >
                                    <i class="icon-checkmark3"></i>
                                    <span>Закрыть</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="panel-group block">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h6 class="panel-title panel-trigger">
                                <a class="collapsed" data-toggle="collapse" href="#question1">Добавленные файлы</a>
                            </h6>
                        </div>

                        <div id="question1" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="attach-files-cont"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h6 class="panel-title"><i class="icon-people"></i> Добавленные пользователи</h6>
                </div>

                <ul class="message-list">
                    @foreach($user_list as $u)
                        <li>
                            <div class="clearfix">
                                <div class="chat-member">
                                    <a href="#">
                                        <img src="{!! !empty($users[$u->userId]->avatar)
                                            ? '/avatars/' . $users[$u->userId]->avatar
                                            : 'http://placehold.it/300' !!}"
                                        >
                                    </a>

                                    <h6>{{ $users[$u->userId]->name }}</h6>
                                </div>

                                <div class="chat-actions">
                                    <a
                                        href="#" title="Начать диалог с {{ $users[$u->userId]->name }}"
                                        onclick="message.newDialogUs({id: '{{ $users[$u->userId]->id }}',
                                            name: '{{ $users[$u->userId]->name }}', check: true})"
                                        class="btn btn-link btn-icon btn-xs"
                                    ><i class="icon-bubble3"></i></a>
                                    <!--a href="#" class="btn btn-link btn-icon btn-xs"><i class="icon-phone2"></i></a-->
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    <div id="deleteComment_modal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="icon-accessibility"></i> Удаление комментария</h4>
                </div>

                <div class="modal-body with-padding">
                    <p>Вы уверены, что хотите удалить комментарий?</p>
                </div>

                <form method="post" action="/comment/delete/">
                    <div class="modal-footer">
                        <button class="btn btn-warning" data-dismiss="modal">Отмена</button>
                        <button type="submit" class="btn btn-primary">Удалить</button>
                        <input type="hidden" name="commentId" value="0" id="deleteCommentInput">
                        <input type="hidden" name="taskId" value="0" id="deleteCommentTaskInput">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
		$(document).ready(function(){
			$('.delete-comment').click(function(){
				$('#deleteCommentInput').val($(this).data('id'));
				$('#deleteCommentTaskInput').val($(this).data('taskid'));
			});
		});
    </script>
@stop