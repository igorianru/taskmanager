@extends('layouts.default')
@section('title','Панель управления')
@section('content')

    @include('layouts.pageHeader', ['pageHeader' => 'Рабочий стол',
    'pageDescription' => $User->name.' вы являетесь ' . $user_role_name . ' в системе, вы можете '. $user_role_text
    ])
    @include('layouts.breadcrumbs',['urls' => [['uri'=>'/','name'=>'Рабочий стол']], 'currentUrl' => 'Все задачи'])
    @include('layouts.errors')

    {{--<div class="panel panel-primary">--}}
        {{--<div class="panel-heading">--}}
            {{--<h6 class="panel-title">--}}
                {{--<a data-toggle="collapse" href="#descriptionProject" class="collapsed">--}}
                    {{--Описание проекта {{  $project->name }}--}}
                {{--</a>--}}
            {{--</h6>--}}
        {{--</div>--}}
        {{--<div id="descriptionProject" class="panel-collapse collapse" style="height: 0px;">--}}
            {{--<div class="panel-body">--}}
                {{--{{ $project->description }}--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    <div class="row block-inner">
        <div class="col-sm-6">
            <div class="well">
                <dl>
                    <select class="form-control" onchange="window.location.href = $(this).val()" autocomplete="off">
                        <option value="/tasks">Все</option>

                        @foreach($projects as $vp)
                            <option value="/tasks/{{ $vp->id }}" @if($vp->id == $id_project) selected @endif>
                                {{ $vp->name }}
                            </option>
                        @endforeach
                    </select>
                </dl>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="well text-right">
                <dl>
                    @if (($User->role == 'A' || $User->role == 'M' || $User->role == 'SA') && $id_project !== null)
                        <a class="btn btn-primary" href="/task/{{ $id_project }}">
                            <i class="icon-plus"></i> Добавить задачу
                        </a>
                    @else
                        <h5>
                            <i class="icon-point-left"></i> Для добавления задачи выберите проект
                        </h5>
                    @endif
                </dl>
            </div>
        </div>
    </div>

    <div class="tabbable page-tabs">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#all-tasks" data-toggle="tab">
                    <i class="icon-paragraph-justify2"></i>
                    Открытые
                    <span class="label label-info">{{ count($tasks_open) }}</span>
                </a>
            </li>

            <li>
                <a href="#active" data-toggle="tab">
                    <i class="icon-play2"></i>
                    В исполении
                    <span class="label label-info">{{ count($tasks_active) }}</span>
                </a>
            </li>

            <li>
                <a href="#closed" data-toggle="tab">
                    <i class="icon-checkmark3"></i>
                    Закрытые
                    <span class="label label-info">{{ count($tasks_closed) }}</span>
                </a>
            </li>

            @if ($id_project !== null)
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-cogs"></i> Управление <b class="caret"></b>
                    </a>

                    <ul class="dropdown-menu">
                        @if ($User->role == 'A' || $User->role == 'M' || $User->role == 'SA')
                            <li>
                                <a href="/task/{{ $id_project }}">
                                    <i class="icon-plus"></i> Добавить задачу
                                </a>
                            </li>
                        @endif

                        @if ($User->role == 'A' || $User->role == 'SA')
                            <li>
                                <a href="/project/edit/{{ $id_project }}" title="Редактировать проект">
                                    <i class="icon-pencil"></i> Редактировать
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
        </ul>

        <div class="tab-content">
            <div class="tab-pane active fade in" id="all-tasks">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h6 class="panel-title"><i class="icon-paragraph-justify2"></i> Задачи</h6>
                    </div>
                    <div class="datatable-tasks">
                        @include('task.all_task_list', ['tasks' => $tasks_open])
                    </div>
                </div>
            </div>

            <div class="tab-pane" id="active">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h6 class="panel-title"><i class="icon-paragraph-justify2"></i> Задачи</h6>
                    </div>
                    <div class="datatable-tasks">
                        @include('task.all_task_list', ['tasks' => $tasks_active])
                    </div>
                </div>
            </div>

            <div class="tab-pane" id="closed">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h6 class="panel-title"><i class="icon-paragraph-justify2"></i> Задачи</h6>
                    </div>
                    <div class="datatable-tasks">
                        @include('task.all_task_list', ['tasks' => $tasks_closed])
                    </div>
                </div>
            </div>

        </div>
    </div>
@stop