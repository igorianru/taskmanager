@extends('layouts.default')
@section('title','Панель управления')
@section('content')
@include('layouts.pageHeader',['pageHeader'=>'Задача','pageDescription'=>'Добавление задачи проекту '. $project->name])
@include('layouts.breadcrumbs',['urls' => [
    ['uri'=>'/','name' => 'Рабочий стол'],
    ['uri'=>'/project/'. $project->id,'name' => $project->name]], 'currentUrl'=>'Задача'
    ])

@include('layouts.errors')
    <form method="post" action="/task/add/{{ $project->id }}" enctype="multipart/form-data">
        <div class="form-group">
            <label>Название задачи</label>
            <input type="text" name="name" value="{!! $old['name'] ?? '' !!}" class="form-control">
        </div>
        <div class="form-group">
            <label>Описание задачи</label>
            <textarea style="min-height: 200px" name="description" class="form-control editor">{!! $old['description'] ?? '' !!}</textarea>
        </div>
        <div class="form-group text-center">
            <button
                class="btn btn-primary"
                style="padding: 20px 70px; margin-bottom: 20px"
                formaction="/task/add/{{ $project->id }}/1" type="submit"
                formmethod="post"
            >
                Включить файлы
            </button>

            <div class="alert alert-warning">
                Внимание! Если включить файлы, то текущие введённые поля сохраняются, затем откроется редактирование,
                в котором можно продолжить создание задачи и прикпрпить/загрузить необходимые файлы
            </div>
        </div>

        <!-- Basic inputs -->
        <div class="panel panel-default">
            <div class="panel-heading"><h6 class="panel-title"><i class="icon-bubble4"></i> Подзадачи</h6></div>
            <div class="panel-body">
                <p>
                    <button
                            data-n="0"
                            type="button"
                            class="add-tasks-btn btn btn-success btn-icon"
                    >
                        <i class="icon-plus"></i>
                    </button>
                </p>

                <div class="tasks-con form-group">
                    @foreach($old['tasks'] ?? [] as $k => $q)
                        <div class="tasksId-{{ $k }} row" style="margin-bottom: 15px">
                            <div class="col-md-10">
                                <input type="text" name="tasks[{{ $k }}][text]"  value="{{ $q['text'] }}" class="form-control" />
                                <input type="hidden" name="tasks[{{ $k }}][status]" value="{{ $q['status'] }}" />
                            </div>

                            <div class="col-md-2">
                                <button type="button" onclick="deleteTasks('.tasksId-{{ $k }}')" class="btn btn-danger btn-icon">
                                    <i class="icon-minus"></i>
                                </button>
                            </div>
                        </div>
                    @endforeach
                </div>

                <script>
                    function deleteTasks (c) {
                        $(c).remove();
                    }

                    $('.add-tasks-btn').click(function () {
                        var n = $(this).data('n');
                        $(this).data('n', n + 1);

                        $('.tasks-con').append('<div class="tasksId-'+ n + ' row" style="margin-bottom: 15px"><div class="col-md-10">' +
                            '<input type="text" name="tasks[' + n + '][text]" class="form-control" />' +
                            '<input type="hidden" name="tasks[' + n + '][status]" value="0" />' +
                            '</div>' +
                            '<div class="col-md-2">' +
                            '<button type="button" onclick="deleteTasks(\'.tasksId-'+ n + '\')" class="btn btn-danger btn-icon">' +
                            '<i class="icon-minus"></i>' +
                            '</button>' +
                            '</div>' +
                            '</div>');
                    })
                </script>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-4">
                <label>Назначить задачу:</label>

                <select name="userAssigned" class="form-control">
                    @foreach($project_users as $user)
                        <option @if($old['userAssigned'] ?? '' == $user->id) selected @endif value="{{ $user->id }}">{{ $user->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-4">
                <label>Приоритет:</label>

                <select name="priority" class="form-control">
                    <option value="low" @if($old['priority'] ?? '' == 'low') selected @endif>Низкий</option>
                    <option value="standard" @if($old['priority'] ?? '' == 'standard') selected @endif>Обычный</option>
                    <option value="high" @if($old['priority'] ?? '' == 'high') selected @endif>Высокий</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-4">
                <label>Крайний срок:</label>
                <input type="text" name="deadline" value="{!! $old['deadline'] ?? '' !!}" class="form-control" data-mask="99/99/9999">
            </div>
        </div>

        <h6 class="heading-hr">Добавить пользователей в задачу</h6>
        <div class="form-group row">
            <div class="col-md-4">
                <label class="clear">Наблюдают:</label>
                <select name="user[]" class="select-multiple select2-offscreen" tabindex="2" multiple>
                    @foreach($project_users as $user)
                        <option @if(array_search($user->id, $old['user'] ?? []) !== false) selected @endif value="{{ $user->id }}">{{ $user->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group text-right">
            <button formmethod="post" class="btn btn-info" formaction="/task/add/{{ $project->id }}/1" type="submit">
                Применить
            </button>

            <input type="submit" class="btn btn-success" value="Добавить">
        </div>
        <input type="hidden" name="_token" value="{{csrf_token()}}">
    </form>
@stop
