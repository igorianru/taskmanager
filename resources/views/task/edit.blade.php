@extends('layouts.default')
@section('title','Панель управления')
@section('content')
    @include('layouts.pageHeader',['pageHeader'=>$task->name,'pageDescription'=>'Редактирование задачи для проекта '.$project->name])
    @include('layouts.breadcrumbs',['urls'=>[
        ['uri'=>'/','name'=>'Рабочий стол'],
        ['uri'=>'/project/'.$project->id,'name'=>$project->name]],'currentUrl'=>$task->name])
    @include('layouts.errors')

    <script src="/js/upl_mul/jquery.uploadifive.min.js" type="text/javascript"></script>
    <script src="/js/lodash.min.js"></script>
    <script src="/js/fileM.js"></script>
    <script>
		$(document).ready(function(){
			$.fileM.initialize({
				csrf_token: '{{ csrf_token() }}',
				id_this_user: '{{ $User->id }}',
				avatar_user: '{{ $User->avatar }}',
				role_user: '{{ $User->role }}',
				name_user: '{{ $User->name }}',
				id_project: '{{ $task->projectId }}',
				id_task: '{{ $task->id }}',
				user_role: '{!! json_encode($user_role) !!}'
			});
		});
    </script>

    <form method="post" action="/task/edit/{{ $task->id }}" enctype="multipart/form-data">
        <div class="form-group">
            <label>Название задачи</label>
            <input type="text" name="name" value="{{ $task->name }}" class="form-control">
        </div>
        <div class="form-group">
            <label>Описание задачи</label>
            <textarea style="min-height: 200px" name="description" class="form-control editor">{{ $task->description }}</textarea>
        </div>

        <!-- Basic inputs -->
        <div class="panel panel-default">
            <div class="panel-heading"><h6 class="panel-title"><i class="icon-bubble4"></i> Подзадачи</h6></div>
            <div class="panel-body">
                <p>
                    <button
                        data-n="{{ count(json_decode($task->tasks, true)) }}"
                        type="button"
                        class="add-tasks-btn btn btn-success btn-icon"
                    >
                        <i class="icon-plus"></i>
                    </button>
                </p>

                <div class="tasks-con form-group">
                    <?Php
                    $tasks_in_task = count(json_decode($task->tasks, true)) > 0
                        ? json_decode($task->tasks, true)
                        : []
                    ?>
                    @foreach($tasks_in_task as $k => $q)
                        <div class="tasksId-{{ $k }} row" style="margin-bottom: 15px">
                            <div class="col-md-10">
                                <input type="text" name="tasks[{{ $k }}][text]"  value="{{ $q['text'] }}" class="form-control" />
                                <input type="hidden" name="tasks[{{ $k }}][status]" value="{{ $q['status'] }}" />
                            </div>

                            <div class="col-md-2">
                                <button type="button" onclick="deleteTasks('.tasksId-{{ $k }}')" class="btn btn-danger btn-icon">
                                    <i class="icon-minus"></i>
                                </button>
                            </div>
                        </div>
                    @endforeach
                </div>

                <script>
					function deleteTasks (c) {
						$(c).remove();
					}

					$('.add-tasks-btn').click(function () {
						var n = $(this).data('n');
						$(this).data('n', n + 1)

						$('.tasks-con').append('<div class="tasksId-'+ n + ' row" style="margin-bottom: 15px"><div class="col-md-10">' +
							'<input type="text" name="tasks[' + n + '][text]" class="form-control" />' +
							'<input type="hidden" name="tasks[' + n + '][status]" value="0" />' +
							'</div>' +
							'<div class="col-md-2">' +
							'<button type="button" onclick="deleteTasks(\'.tasksId-'+ n + '\')" class="btn btn-danger btn-icon">' +
							'<i class="icon-minus"></i>' +
							'</button>' +
							'</div>' +
							'</div>');
					})
                </script>
            </div>
        </div>

        <div class="form-group">
            <div class="panel-group block">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h6 class="panel-title panel-trigger">
                            <a class="collapsed" data-toggle="collapse" href="#question1">Прикреплённые файлы</a>
                        </h6>
                    </div>

                    <div id="question1" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p>
                                <a
                                    data-toggle="modal"
                                    role="button"
                                    href="#file-modal"
                                    data-target="#file-modal"
                                    class="add-tasks-btn btn btn-success btn-icon"
                                >
                                    <i class="icon-plus"></i>
                                </a>
                            </p>

                            <input type="hidden" name="attachment" value="{{ $task->attachment }}" autocomplete="off" />
                            <div class="attach-files-cont"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-4">
                <label>Назначить задачу:</label>
                <select name="userAssigned" class="form-control">
                    @foreach($allUsers as $user)
                        <option value="{{ $user->id }}" @if($task->userAssigned == $user->id) selected @endif >{{ $user->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-4">
                <label>Приоритет:</label>

                <select name="priority" class="form-control">
                    <option value="low" @if($task->priority == 'low') selected @endif >Низкий</option>
                    <option value="standard" @if($task->priority == 'standard') selected @endif >Обычный</option>
                    <option value="high" @if($task->priority == 'high') selected @endif >Высокий</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-4">
                <label>Крайний срок:</label>
                <input type="text" name="deadline" value="{{ $task->deadline }}" class="form-control" data-mask="99/99/9999">
            </div>
        </div>

        <h6 class="heading-hr">Добавить пользователей в задачу</h6>
        <div class="form-group row">
            <div class="col-md-4">
                <label class="clear">Наблюдают:</label>

                <select name="user[]" class="select-multiple select2-offscreen" tabindex="2" multiple>
                    @foreach($allUsers as $user)
                        <option value="{{ $user->id }}" @if(isset($user_list[$user->id])) selected @endif>
                            {{ $user->name }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group text-right">
            <a class="btn btn-warning" href="/task/detailed/{{ $task->id }}" type="submit" >Отменить</a>

            <button formmethod="post" class="btn btn-info" formaction="/task/edit/{{ $task->id }}/1" type="submit" >
                Применить
            </button>

            <input type="submit" class="btn btn-success" value="Сохранить">
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
    </form>
@stop