$(function ($) {
	$.guiS = {
		url: '/',

		initialize: function initialize(data) {
			this.conf = data;
			this.csrf_token = this.conf.csrf_token;

			var html = '<div id="guiModal" class="modal fade guiModal" tabindex="-1" role="dialog">' +
				'<div class="modal-dialog modal-sm">' +
				'<div class="modal-content">' +
				'<div class="modal-header">' +
				'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
				'<h4 class="modal-title"></h4>' +
				'</div>' +
				'<div class="modal-body with-padding"></div>' +
				'<div class="modal-footer">' +
				'<button class="btn btn-warning submit-close" data-dismiss="modal">Отменить</button>' +
				'<button type="submit" class="btn btn-primary submit-send">Сохранить</button>' +

				'<input type="hidden" name="actions" value="">' +
				'<input type="hidden" name="id" value="">' +
				'<input type="hidden" name="table" value="">' +
				'<input type="hidden" name="param" value="">' +
				'<input type="hidden" name="ids" value="">' +
				'<input type="hidden" name="project_id" value="">' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</div>';

			$('body').append(html);
		},

		sendAjax: function sendAjax (callback) {
			var d = JSON.stringify({
				'_token': $.guiS.csrf_token,
				'actions': $('[name="actions"]').val(),
				'id': $('[name="id"]').val(),
				'table': $('[name="table"]').val(),
				'param': $('[name="param"]').val(),
				'ids': $('[name="ids"]').val()
			});

			$.ajax
			({
				type: "post",
				url: "/main/gui-send",
				data: d,
				cache: false,
				contentType: "application/json; charset=utf-8",
				dataType: "JSON",
				success: function (data) {
					if (data['result'] == "ok") {
						var projectId = $('[name="project_id"]').val();

						$.guiS[callback]('callback', data['param']);
						$('#guiModal').modal('hide');

						if(projectId) return window.location.href = '/tasks/' + projectId;
					} else {
						// error message
					}
				}
			})
		},

		/**
		 * processes project inwork \ paused
		 * @param id
		 * @param s
		 */
		processesTask: function (id, s) {
			if(id === 'callback') {
				id = $('[name="id"]').val();

				$('.id-p-' + $('[name="id"]').val() + ' .project-BB > li > a').removeClass('activeBB');
				var classPrss = $('.id-p-' + $('[name="id"]').val() + ' .project-' + s + '-li');
				var classPrssT = $('.project-status-li-text');

				if(s == 'inwork') {
					classPrss
						.html('<a href="#" class="activeBB"><i class="icon-pause"></i> <span>In work</span></a>')
						.attr({'onclick': '$.guiS.processesTask(' + id + ', \'paused\')'});

					classPrssT.html('In work');
				}

				if(s == 'paused') {
					classPrss
						.html('<a href="#"><i class="icon-play2"></i> <span>In work</span></a>')
						.attr({'onclick': '$.guiS.processesTask(' + id + ', \'inwork\')'});

					classPrssT.html('Приостановлена');
				}

				if(s == 'review') {
					classPrss
						.html('<a href="#" class="activeBB"><i class="icon-tv"></i> <span>Review</span></a>')
						.attr({'onclick': '$.guiS.processesTask(' + id + ', \'review\')'});

					classPrssT.html('В предосмотре');
				}

				if(s == 'test') {
					classPrss
						.html('<a href="#" class="activeBB"><i class="icon-spinner7"></i> <span>Test</span></a>')
						.attr({'onclick': '$.guiS.processesTask(' + id + ', \'test\')'});

					classPrssT.html('В тестировании');
				}

				if(s == 'closed') {
					classPrss
						.html('<a href="#" class="activeBB"><i class="icon-checkmark3"></i> <span>Закрыть</span></a>')
						.attr({'onclick': '$.guiS.processesTask(' + id + ', \'closed\')'});

					classPrssT.html('Закрытая');
				}

				return;
			}

			$('[name="actions"]').val('processes_project');
			$('[name="id"]').val(id);
			$('[name="table"]').val('tasks');
			$('[name="param"]').val(s);

			setTimeout(function () {
				$.guiS.sendAjax('processesTask');
			}, 0)
		},

		/**
		 * status project active \ closed
		 * @param id
		 * @param s
		 */
		statusProject: function (id, s) {
			if(id === 'callback') {
				var classPrj = $('.id-p-' + $('[name="id"]').val() + ' .project-status-li > a');
				var classPrjT = $('.id-p-' + $('[name="id"]').val() + ' .project-status-li-text');

				if(s == 'closed') {
					classPrj
						.html('<i class="icon-play3"></i>')

						.attr({
							'title': 'Возобновить проект',
							'onclick': '$.guiS.statusProject(' + id + ', \'active\')'
						});

					classPrjT.html('<a title="Проект закрыт">Проект закрыт</a>');
				} else {
					classPrj
						.html('<i class="icon-stop2"></i>')

						.attr({
							'title': 'Завершить проект',
							'onclick': '$.guiS.statusProject(' + id + ', \'closed\')'
						});

					classPrjT.html('<a title="Активный проект">Активный проект</a>');
				}

				return;
			}

			if(s == 'active') {
				$('.guiModal .modal-title').html('<i class="icon-meter-fast"></i> Возобновление проекта');
				$('.guiModal .modal-body').html('<p>Вы уверены, что хотите возобновить проект?</p>');
				$('.guiModal .submit-send').html('Возобновить').attr('onclick', '$.guiS.sendAjax(\'statusProject\')');
			} else {
				$('.guiModal .modal-title').html('<i class="icon-meter-slow"></i> Остановка проекта');
				$('.guiModal .modal-body').html('<p>Вы уверены, что хотите остановить проект?</p>');
				$('.guiModal .submit-send').html('Остановить').attr('onclick', '$.guiS.sendAjax(\'statusProject\')');
			}

			$('[name="actions"]').val('status_project');
			$('[name="id"]').val(id);
			$('[name="table"]').val('projects');
			$('#guiModal').modal('show');
		},

		/**
		 * delete project
		 * @param id
		 */
		deleteProject: function (id) {
			if(id === 'callback') {
				$('.id-p-' + $('[name="id"]').val()).remove();

				return;
			}

			$('.guiModal .modal-title').html('<i class="icon-remove2"></i> Удаление проекта');
			$('.guiModal .modal-body').html('<p>Вы уверены, что хотите удалить проект?</p>');
			$('.guiModal .submit-send').html('Удалить').attr('onclick', '$.guiS.sendAjax(\'deleteProject\')');
			$('[name="actions"]').val('delete_project');
			$('[name="id"]').val(id);
			$('[name="table"]').val('project');
			$('#guiModal').modal('show');
		},

		/**
		 * delete task
		 * @param id
		 * @param projectId
		 */
		deleteTask: function (id, projectId) {
			if(id === 'callback') {
				$('.id-t-' + $('[name="id"]').val()).remove();

				return;
			}

			$('.guiModal .modal-title').html('<i class="icon-remove2"></i> Удаление задачи');
			$('.guiModal .modal-body').html('<p>Вы уверены, что хотите удалить задачу?</p>');
			$('.guiModal .submit-send').html('Удалить').attr('onclick', '$.guiS.sendAjax(\'deleteTask\')');
			$('[name="actions"]').val('delete_task');
			$('[name="id"]').val(id);
			$('[name="project_id"]').val(projectId);
			$('[name="table"]').val('tasks');
			$('#guiModal').modal('show');
		},

		/**
		 * closing task
		 * @param id
		 */
		closeTask: function (id) {
			if(id === 'callback') {
				$('.id-t-' + $('[name="id"]').val() + ' .label-status').html('<span class="label label-default">' +
					'Закрыта</span>');

				$('.id-t-' + $('[name="id"]').val() + ' .close-task-li').remove();

				return;
			}

			$('.guiModal .modal-title').html('<i class="icon-checkmark3"></i> Закрыть задачу');
			$('.guiModal .modal-body').html('<p>Вы уверены, что хотите закрыть задачу?</p>');
			$('.guiModal .submit-send').html('Закрыть задачу').attr('onclick', '$.guiS.sendAjax(\'closeTask\')');
			$('[name="actions"]').val('close_task');
			$('[name="id"]').val(id);
			$('[name="table"]').val('tasks');
			$('#guiModal').modal('show');
		},

		/**
		 * checked task
		 * @param id
		 * @param s
		 * @param ids
		 */
		checkedTask: function (id, s, ids) {
			if(id === 'callback') {
				if(s == 'checked') {
					$('.checked-' + $('[name="ids"]').val()).css({textDecoration: 'line-through'});
				} else {
					$('.checked-' + $('[name="ids"]').val()).css({textDecoration: 'inherit'});
				}

				return;
			}

			$('[name="actions"]').val('checked_task');
			$('[name="id"]').val(id);
			$('[name="table"]').val('tasks');
			$('[name="param"]').val(s);
			$('[name="ids"]').val(ids);

			setTimeout(function () {
				$.guiS.sendAjax('checkedTask');
			}, 0)
		}
	}
});