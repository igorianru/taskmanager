$(function ($) {
	$.fileM = {
		url: '/',

		initialize: function initialize(data) {
			var html = '';

			this.conf = data;
			this.csrf_token = this.conf.csrf_token;
			this.id_project = this.conf.id_project;
			this.id_task = this.conf.id_task;
			this.attachCont = this.conf.attachCont || '.attach-files-cont';
			this.attachInput = this.conf.attachInput || '[name=attachment]';
			this.dataAll = this.conf.dataAll || [];
			this.filesInModal = [];

			this.attach = $(this.attachInput).val()
				? $(this.attachInput).val().split(',')
				: (this.conf.attach || '').split(',');

			this.openModal = this.conf.openModal || '.body-img-modal';
			this.type_mini = this.conf.type_mini || false;

			if(!this.type_mini) {
				html += '<div id="file-modal" class="modal fade" tabindex="-1" role="dialog">' +
					'<div class="modal-dialog modal-lg">' +
					'<div class="modal-content">' +
					'<div class="modal-header">' +
					'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
					'<h4 class="modal-title">Файлы</h4>' +
					'</div>' +

					'<div class="panel">' +
					'<div class="panel-body">' +

					'<div class="row invoice-header">' +
					'<div class="col-sm-6">' +
					'<h3>Все загруженные файлы</h3>' +
					'<span>Файлы из всех проектов к которым у вас есть доступ</span>' +
					'</div>' +

					'<div class="col-sm-6">' +
					'<ul class="invoice-details">' +
					'<li>Вами использовано : <strong class="text-danger file-all-size"></strong></li>' +
					'<li>Всего ваших файлов: <strong class="file-all"></strong></li>' +
					'</ul>' +
					'</div>' +

					'<div class="col-sm-12">' +
					'<div class="text-danger">' +
					'При удалении файла, он удалится навсегда, так же более будет не доступен в' +
					'проектах где он был когда либо прикреплён' +
					'</div>' +
					'</div>' +
					'</div>' +

					'<div class="row">' +
					'<div class="col-sm-12">' +
					'<form style="margin-bottom: 5px">' +
					'<div id="queue" class="alert queue"></div>' +
					'<input id="file_upload" name="file_upload" type="file" multiple="multiple">' +
					'<span class="help-block">' +
					'Разрешены все форматы, кроме исполняющих файлов' +
					'</span>' +
					'</form>' +

					'<div class="breadcrumb-line">' +
					'<div class="help-block_m">В этом проекте | Всех пользователей</div>' +
					'<ul class="breadcrumb-buttons collapse">' +
					'<li class="dropdown">' +
					'<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-search3"></i> <span>Search</span> <b class="caret"></b></a>' +
					'<div class="popup dropdown-menu dropdown-menu-right">' +
					'<div class="popup-header">' +
					'<span>Быстрый поиск</span>' +
					'</div>' +
					'<form action="#" class="breadcrumb-search">' +
					'<input placeholder="Введите текст и нажмите enter..." name="search-file" class="form-control autocomplete ui-autocomplete-input" autocomplete="off" type="text">' +
					'<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>' +
					'<div class="row">' +
					'<div class="col-xs-12">' +
					'<label class="radio">' +
					'<div class="choice1"><span class="checked"><input name="search-project" value="this" class="styled" checked="checked" type="radio"></span></div>' +
					'В этом проекте' +
					'</label>' +
					'<label class="radio">' +
					'<div class="choice1"><span><input name="search-project" value="all" class="styled" type="radio"></span></div>' +
					'Во всех проектах' +
					'</label>' +
					'</div>' +

					'<div class="col-xs-12">' +
					'<label class="radio">' +
					'<div class="choice1"><span><input name="search-users" value="this" class="styled" type="radio"></span></div>' +
					'Загруженные мной' +
					'</label>' +
					'<label class="radio">' +
					'<div class="choice1"><span><input name="search-users" value="all"  checked="checked" class="styled" type="radio"></span></div>' +
					'Всех пользователей' +
					'</label>' +
					'</div>' +
					'</div>' +

					'<input class="btn btn-block btn-success searchFiles" value="Искать" type="button">' +
					'</form>' +
					'</div>' +
					'</li>' +
					'</ul>' +
					'</div>' +

					'</div>' +
					'</div>' +

					'<div class="row cont-file"></div>' +
					'<div class="cont_pagination"></div>' +
					'<input type="hidden" class="pagination" value="0">' +
					'</div>' +
					'</div>' +

					'<div class="modal-footer">' +
					'<button type="button" class="btn btn-default btn-attach-close" data-dismiss="modal">Закрыть</button>' +
					'<button type="button" class="btn btn-primary btn-attach" disabled>Прикрепить</button>' +
					'</div>' +
					'</div>' +
					'</div>' +
					'</div>';

				setTimeout(function () {
					$.fileM.loadUploadLib();
				}, 10);

				setTimeout(function () {
					$.fileM.loadFile();
					// $.uniform.update('.styled');
					// $(".styled").uniform({ radioClass: 'choice' });
					$(".styled").uniform({ radioClass: 'choice' });
				}.bind(this), 100);
			}

			html += '<!-- Modal -->' +
				'<div class="modal fade modalgal" id="galleryc2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">' +
				'<div class="modal-dialog modal-lg" role="document">' +
				'<div class="modal-content ">' +
				'<div class="modal-body">' +
				'<div class="modal-header">' +
				'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
				'<h4 class="modal-title">Просмотр изображения</h4>' +
				'</div>' +
				'<div id="cCarousel" class="carousel slide">' +
				'<div class="windows8T">' +
				'<div class="windows8">' +
				'<div class="wBall" id="wBall_1">' +
				'<div class="wInnerBall"></div>' +
				'</div>' +
				'<div class="wBall" id="wBall_2">' +
				'<div class="wInnerBall"></div>' +
				'</div>' +
				'<div class="wBall" id="wBall_3">' +
				'<div class="wInnerBall"></div>' +
				'</div>' +
				'<div class="wBall" id="wBall_4">' +
				'<div class="wInnerBall"></div>' +
				'</div>' +
				'<div class="wBall" id="wBall_5">' +
				'<div class="wInnerBall"></div>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'<div class="carousel-inner body-img-modal"></div>' +
				'<a class="mleft left carousel-control" href="#cCarousel" role="button" data-slide="prev">' +
				'<div class="icon-arrow-left"></div>' +
				'<span class="sr-only">Previous</span>' +
				'</a>' +
				'<a class="mright right carousel-control" href="#cCarousel" role="button" data-slide="next">' +
				'<div class="icon-arrow-right2"></div>' +
				'<span class="sr-only">Next</span>' +
				'</a>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</div>' +

				'<!-- Modal -->' +
				'<div class="modal fade" id="modalDel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' +
				'<div class="modal-dialog">' +
				'<div class="modal-content">' +
				'<div class="modal-header">' +
				'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
				'<h4 class="modal-title modal-title-mess" id="myModalLabel">Удалить</h4>' +
				'</div>' +
				'<div class="modal-body modal-body-mess"></div>' +
				'<div class="modal-footer">' +
				'<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>' +
				'<button type="button" class="btn btn-danger delbMod">Удалить</button>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</div>';

			$('body').append(html);

			setTimeout(function () {
				this.loadOnclick();
			}.bind(this), 100)
		},

		loadOnclick: function () {
			// remove attach to click on close
			$('.btn-attach-close').click(function () { $.fileM.attachRemove(); });

			// attach file and remove active files
			$('.btn-attach').click(function () { $.fileM.attachFile(); });

			// reload attach
			$('.pagination').click(function () { _.map(this.attach, function (v) { this.attachment(v); }); });

			// load current file
			this.loadFile(true);

			// carousel modal
			$('#cCarousel').on('slid.bs.carousel.active', function (e) {
				$('.item-' + $(this).find('.active').data('idT')).html('<img data-holder-rendered="true" src=" ' + $(this).find(".active").data("src") + '" />');
			});

			$('#cCarousel').carousel('pause');

			// search
			this.searchFiles();
		},

		gal: function (s, c, f) {
			if (!c) c = '.item';
			$(c).removeClass('active');
			$(s).addClass('active');
			$('.item-' + $(s).data('idT')).html('<img data-holder-rendered="true" src="' + $(s).data('src') + '" />');
			if ($(s + '-c').data('name')) {
				$('.name-w').html($(s + '-c').data('name'));
			} else {
				$('.name-w').html('');
			}
		},

		attachRemove: function () {
			_.map(this.attach, function (v) { $('.boxed-' + v).removeClass('active'); });
			this.attach = [];
			$('.btn-attach').attr('disabled', 'disabled')
		},

		attachFile: function () {
			var attachInput =  $(this.attachInput).val().split(',');

			_.map(this.attach, function (v) {
				var find = _.find(this.dataAll, {id: v});

				if(find && _.indexOf(attachInput, v.toString()) === -1)
					$(this.attachCont).append(this.addFile([find], true));
			}.bind(this));

			$(this.attachInput).val(this.attach.join(','));
			$('#file-modal').modal('hide');
		},

		/**
		 * @param id
		 * @param activeOnly
		 */
		attachment: function (id, activeOnly) {
			var b = $('.boxed-' + id);

			if(b.hasClass("active")) {
				b.removeClass('active');
				if(!activeOnly) this.attach = _.pull(this.attach, id);
			} else {
				b.addClass('active');
				if(!activeOnly) this.attach = _.concat(this.attach, id);
			}

			if(!activeOnly) {
				if(!_.isEmpty(this.attach)) {
					$('.btn-attach').removeAttr('disabled')
				} else {
					$('.btn-attach').attr('disabled', 'disabled')
				}
			}
		},

		searchFiles: function () {
			$('.searchFiles').click(function () {
				this.loadFile(false, {
					project: $('[name=search-project]:checked').val(),
					search : $('[name=search-file]').val(),
					users  : $('[name=search-users]:checked').val(),
				});

				this._updateSearchParams();
			}.bind(this));

			this._updateSearchParams();
		},

		_updateSearchParams: function () {
			var project = $('[name=search-project]:checked').val() === 'all' ? 'Во всех проектах' : 'В этом проекте';
			var users = $('[name=search-users]:checked').val() === 'all' ? 'Всех пользователей' : 'Загруженные мной';

			$('.help-block_m').html(project + '|' + users);
		},

		unAttachment: function (id) {
			$('.ts-boxed' + id).remove();
			$('.boxed-' + id).removeClass('active');
			this.attach = _.pull(this.attach, id.toString());
			$(this.attachInput).val(this.attach.join(','));

			if(!_.isEmpty(this.attach)) {
				$('.btn-attach').removeAttr('disabled')
			} else {
				$('.btn-attach').attr('disabled', 'disabled')
			}
		},

		/**
		 * loadUploadLib
		 */
		loadUploadLib: function () {
			var typeFile = [
				'image/jpg',
				'image/gif',
				'image/GIF',
				'image/png',
				'image/jpeg',
				'image/JPG',
				'image/PNG',
				'image/JPEG',
				'application/octet-stream',
				'*excel',
				'*txt',
				'*doc',
				'application/pdf',
			];

			$('#file_upload').uploadifive({
				formData: {
					_token    : this.csrf_token,
					id_task   : this.id_task ,
					id_project: this.id_project
				},

				debug        : true,
				queueID      : 'queue',
				buttonText   : 'Выбрать изображения',
				buttonClass  : 'btn btn-primary imag_bat',
				width        : 350,
				height       : 40,
				lineHeight   : '20px',
				fileType     : typeFile,
				fileDesc     : 'All supported files types (.pdf, .jpeg)',
				uploadScript : '/files/load_files',
				onProgress   : 'total',
				fileSizeLimit: '10048KB',

				'onUploadComplete' : function(file, data)
				{
					$.fileM.loadFile();
				}
			});
		},

		/**
		 * @param data
		 * @param simple
		 * @returns {string}
		 */
		addFile: function (data, simple) {
			var html = '', boxedM, boxedC;
			if(!_.find(this.dataAll, data)) this.dataAll = _.concat(this.dataAll, data);

			for(var i = 0; data.length > i; i++) {
				var d = data[i];
				boxedM = !simple ? 'fl-boxed' : 'ts-boxed';
				boxedC = !this.type_mini ? 'col-lg-3 col-md-6 col-sm-6' : 'col-lg-4 col-md-6 col-sm-6';

				if(d['type'] === 'images') {
					html += '<div class="' + boxedC + boxedM + d['id'] + '">' +
						'<div class="block">' +
						'<div class="thumbnail thumbnail-boxed boxed-' + d['id']  + '" >' +
						'<div class="thumb">' +
						'<img alt="" src="/images/files/small/' + d['file'] + '">' +
						'<div class="thumb-options">' +
						'<span style="padding: 5px;">';

					if(!simple) {
						html += '<a href="javascript:void(0)" class="btn btn-icon btn-success" onclick="$.fileM.attachment(' + d['id']  + ')" title="Прикрепить" data-id="' + d['id']  + '">' +
							'<i class="icon-attachment"></i>' +
							'</a>' +
							'<a href="javascript:void(0)" class="btn btn-icon btn-success" onclick="$.fileM.rowDelete({i: ' + d['id']  + ', t: \'files\'})" title="Удалить">' +
							'<i class="icon-remove"></i>' +
							'</a>';
					} else {
						if(!this.type_mini) {
							html += '<a href="javascript:void(0)" class="btn btn-icon btn-success" onclick="$.fileM.unAttachment(' + d['id']  + ')" title="Открепить">' +
								'<i class="icon-minus"></i>' +
								'</a>';
						}
					}
						// '<a href="#" class="btn btn-icon btn-success" title="Редактировать">' +
						// '<i class="icon-quill2"></i>' +
						// '</a>' +
					html += '<a href="javascript:void(0)" class="btn btn-icon btn-success" title="Увеличить"' +
						' data-toggle="modal"' +
						' onclick="$.fileM.gal(\'.item-' + d['id']  + '\', \'.itemC\', \'' + boxedM + '\')"' +
						' data-target="#galleryc2">' +
						'<i class="icon-zoom-in"></i>' +
						'</a>' +
						'<a href="javascript:void(0)" onclick="$.fileM.href(\'/files/get_file/\' + ' + d['id']  + ')" class="btn btn-icon btn-success" title="Скачать">' +
						'<i class="icon-download2"></i>' +
						'</a>' +
						'<p style="color: #fff; word-break: break-all;">' +
						'<br />' +
						d['orig_name'] +
						'</p>' +
						'</span>' +
						'</div>' +
						'</div>' +
						'</div>' +
						'</div>' +
						'</div>';
				} else {
					html += '<div class="' + boxedC + boxedM + d['id'] + '">' +
						'<div class="block">' +
						'<div class="thumbnail thumbnail-boxed boxed-' + d['id']  + '">' +
						'<div class="thumb" style="padding: 60px; width: 100%;">' +
						'<i class="icon-file7" style="font-size: 74px;"> </i>' +
						'<div class="thumb-options">' +
						'<span style="padding: 5px;">';

					if(!simple) {
						html += '<a href="javascript:void(0)" class="btn btn-icon btn-success" onclick="$.fileM.attachment(' + d['id']  + ')" title="Прикрепить" data-id="' + d['id']  + '">' +
							'<i class="icon-attachment"></i>' +
							'</a>' +
							'<a href="javascript:void(0)" class="btn btn-icon btn-success" onclick="$.fileM.rowDelete(\'' + d['id']  + '\', \'files\')" title="Удалить">' +
							'<i class="icon-remove"></i>' +
							'</a>';
					} else {
						if(!this.type_mini) {
							html += '<a href="javascript:void(0)" class="btn btn-icon btn-success" onclick="$.fileM.unAttachment(' + d['id']  + ')" title="Открепить">' +
								'<i class="icon-minus"></i>' +
								'</a>';
						}
					}
						// '<a href="#" class="btn btn-icon btn-success"><i class="icon-quill2"></i></a>' +
					html += '<a href="javascript:void(0)" onclick="$.fileM.href(\'/files/get_file/\' + ' + d['id']  + ')" class="btn btn-icon btn-success" title="Скачать">' +
						'<i class="icon-download2"></i>' +
						'</a>' +
						'<p style="color: #fff; word-break: break-all;">' +
						'<br />' +
						d['orig_name'] +
						'</p>' +
						'</span>' +
						'</div>' +
						'</div>' +
						'</div>' +
						'</div>' +
						'</div>';
				}
			}

			return html;
		},

		addFileModal: function (data, clear) {
			var html = '';
			if(clear) $(this.openModal).html('');

			for(var i = 0; data.length > i; i++) {
				var
					d = data[i],
					hash = _.indexOf(this.filesInModal, d.id);

				this.filesInModal = !hash ? _.pull(this.filesInModal, d.id) : _.concat(this.filesInModal, d.id);

				if(hash && d['type'] === 'images') {
					html = '<div' +
						' data-id-t="' + d.id  + '-r"' +
						' data-src="/images/files/original/' + d.file  + '"' +
						' class="itemC item thumbnail item-' + d.id  + '  item-' + d.id  + '-r"' +
						' style="margin-bottom: 0"></div>';

					$(this.openModal).append(html);
				}
			}
		},

		href: function (url, target) {
			if(target) return window.open(url, '_blank');

			return window.location.href = url;
		},

		rowDelete: function (o) {
			$('#modalDel').modal('show');
			$('.delbMod').attr('onclick','$.fileM.rowDeleteOk('+ JSON.stringify(o) +')');
			$(".modal-body-mess").html('<h3 class="text-center">Вы уверены?</h3>');
			$(".modal-title-mess").html('Удалить');
		},

		rowDeleteOk: function (o) {
			$.ajax({
				cache: false,
				url: '/files/row_delete',
				data: '_token=' + $.guiS.csrf_token + '&id=' + o.i + '&table=' + o.t,
				type: 'post',
				dataType: 'JSON',

				success: function(data) {
					$('#modalDel').modal('hide');
					if(data['mess'] === '' || data['mess'] === 'ok') $('.fl-boxed' + o.i).remove();
				}
			});
		},

		/**
		 * @param loadCurrentFile - file current task
		 * @param search - object search
		 */
		loadFile: function (loadCurrentFile, search) {
			$.ajax
			({
				type: "post",
				url: "/files/get_files",

				data: JSON.stringify({
					_token     : $.guiS.csrf_token,
					attachment : this.attach,
					currentFile: loadCurrentFile,
					id_project : this.id_project,
					id_task    : this.id_task,
					page       : _.isEmpty(search) ? $('.pagination').val() : 0,
					search     : search || {
						project: $('[name=search-project]:checked').val(),
						search : $('[name=search-file]').val(),
						users  : $('[name=search-users]:checked').val(),
					},
				}),

				cache: false,
				contentType: "application/json; charset=utf-8",
				dataType: "JSON",

				success: function (data) {
					if (data['result'] === "ok") {
						var dataFile;

						if(!loadCurrentFile) {
							dataFile = _.get(data, 'data.data', []);

							$('.file-all').html(_.get(data, 'files_info.count'));
							$('.file-all-size').html(Math.round(_.get(data, 'files_info.size') / 1048576) + 'Mb');

							$.fileM.addFileModal(dataFile)
							$('.cont-file').html($.fileM.addFile(dataFile));
							$('.cont_pagination').html('<div class="text-center clear">' + data.pagination + '</div>');
							_.map($.fileM.attach, function (v) { $.fileM.attachment(v, true); });

							setTimeout(function () {
								$('.pagination').click(function (ev) {
									var page = ev.target.innerText;

									if(data.data.current_page == page)  return false;

									if(!_.isNaN(parseInt(page))) {
										$('.pagination').val(page);
										$.fileM.loadFile();
									}

									if((data.data.current_page + 1) <= data.data.last_page) {
										if(page == '»') {
											nexP = data.data.current_page + 1;
											$('.pagination').val(nexP);
											$.fileM.loadFile();
										}
									}

									if((data.data.current_page - 1) > 0) {
										if(page == '«') {
											nexP = data.data.current_page - 1;
											$('.pagination').val(nexP);
											$.fileM.loadFile();
										}
									}

									return false;
								})
							}, 100)
						} else {
							dataFile = _.get(data, 'data.data', []);

							$.fileM.addFileModal(dataFile)
							$($.fileM.attachCont).append($.fileM.addFile(dataFile, true));
						}
					} else {
						// error message
					}
				}
			})
		}
	}
});