var message = {
    url: 'http://91.240.87.136:8013/',
    user: undefined,
    conf: undefined,
    primus: undefined,

    id_this_user: undefined,
    avatar_user: undefined,
    role_user: undefined,
    name_user: undefined,

    initialize: function initialize(data) {
        this.conf = data;
        this.primus = this.conf['primus'];
        this.id_this_user = this.conf['id_this_user'];
        this.role_user = this.conf['role_user'];
        this.name_user = this.conf['name_user'];
        this.csrf_token = this.conf.csrf_token;

        try{
            this.user_role = JSON.parse(this.conf.user_role);
        } catch(e) {
            // error
            this.user_role = {}
        }

        if(this.conf['avatar_user']) {
            this.avatar_user= '<img src="/avatars/' + this.conf['avatar_user'] + '" alt="">';
        } else {
            this.avatar_user = '<img src="http://placehold.it/300" alt="">';
        }

        $(".newDialogUs").click(function () {
            message.newDialogUs(JSON.parse($(this).attr('data-user')));
        });
        $(".closeTab").click(function () {
            message.closeTab($(this).attr('data-id'));
        });

        this.showDialog(0);



        if(this.getMassUser('all') != undefined) {
            var m = this.getMassUser('all');
            for (var i in m) {
                this.newDialogUs({"id":""+ m[i]['id']+ "", "name":""+ m[i]['name']+ ""});
            }
        }
    },
    resizeWin: function resizeWin() {
        var height = $(window).height();
        $('.chat').css({maxHeight: height - 440, height: height - 440})
    },
    rouMess: function rouMess(data) {
        switch (data.method) {
            case 'enter_mess':
                this.messUp(data.param);

                break;
            case 'load_dialogs':
                this.showDialog(data.param);

                break;
            case 'read_mess_dialog':
                this.readMess(data.param, 1);

                break;
            case 'new_message':
                this.newMessage(data.param);

                break;
            case 'notices_message':
                this.noticesMessage(data.param);
                this.messUp(data.param);

                break;
        }
    },
// new dialog
    newDialogUs: function newDialogUs(data) {
        var userData = $.session.get('user_tab');

            try{
                this.user = JSON.parse(userData);
            } catch(e) {
                // error
                $.session.remove('user_tab');
                this.user = undefined;
            } finally {
                //$.session.remove('user_tab');
                //this.user = undefined;
            }

        // тут сделать очерёдность
        this.addToSess(data);

        if ((userData == undefined || userData ==  '{}') || typeof userData[data.id] == undefined) {
            this.addDialogForm(data);
        } else {
            if($('#usertab-' + data.id).html() == undefined) {
                this.addDialogForm(data);
            } else {
                this.openDialogForm(data);
            }
        }

        this.resizeWin();

        if(data.check) {
            var parser = document.createElement('a');
            parser.href = document.location.href;

            if(!parser.pathname.indexOf('messages') + 1) {
                document.location.href = '/messages#usertab-' + data.id;
            }
        }
    },

// обалочка для диалога
    addDialogForm: function addDialogForm(data) {
        $("#navDialTop").append(
            '<li class="usertab-' + data.id + 'li">' +
            '<a href="#usertab-' + data.id + '" data-toggle="tab">' +
            data.name +
            '<span class="status status-danger"></span>' +
            '&nbsp; &nbsp; <button type="button" class="close closeTab" data-id="' + data.id + '" onclick="message.closeTab('+data.id+')">×</button>' +
            '</a>' +
            '</li>');

        $("#navDialBottom").append('<div class="tab-pane fade" id="usertab-' + data.id + '">' +

            '<div class="panel-collapse collapse in">' +
            '<div class="chat"><div></div></div>' +
            '<textarea name="enter-message" onkeydown="if(event.keyCode==13){message.enterMessage(this); return false;}" class="form-control enter-message" rows="3" cols="1" placeholder="Введите сообщение..."></textarea>' +
            '<div class="message-controls">' +
            '<div class="pull-right">' +
            '<div class="upload-options">' +
            '<a href="#" title="" class="tip" data-original-title="Smileys"><i class="icon-smiley"></i></a>' +
            '<a href="#" title="" class="tip" data-original-title="Upload photo"><i class="icon-camera3"></i></a>' +
            '<a href="#" title="" class="tip" data-original-title="Attach file"><i class="icon-attachment"></i></a>' +
            '</div>' +
            '<button type="button" class="btn btn-success btn-loading enterMessage" onclick=" message.enterMessage(this)" data-loading-text="<i class=\'icon-spinner7 spin\'></i> Processing">Отправить</button>' +
            '</div>' +
            '</div>' +
            '</div>' +

            '</div>');

        $('.usertab-' + data.id + 'li > a').click();

        $('#see_dialog_li').fadeTo(300, 1);
        $('#see_dialog_li > a').click();

        this.loadDialog(data.id, 0);
    },
    openDialogForm: function openDialogForm(data) {
        $('.usertab-' + data.id + 'li > a').click();

        $('#see_dialog_li').fadeTo(300, 1);
        $('#see_dialog_li > a').click();
    },
    closeTab: function closeTab(id) {
        var userData = $.session.get('user_tab');

        if (userData != undefined) {
            this.user = JSON.parse(userData);
            var before = delete this.user[id];
            $.session.set('user_tab', JSON.stringify(this.user));

            $('.usertab-' + id + 'li').remove();
            $('#usertab-' + id).remove();
            userData = $.session.get('user_tab');

            if(userData == '{}') {
                $('.last_dialog').click();
                $('#see_dialog_li').fadeTo(300, 0);
            } else {
                userData = JSON.parse(userData);
                var res = [];

                for (var i in userData) {
                    res.push(i);
                }

                $('.usertab-' + res + 'li > a').click();
            }
        }
    },
    addToSess: function addToSess(data) {
        var userData = $.session.get('user_tab');
        if (userData == '{}') {
            userData = undefined;
        }

        if (userData != undefined) {
            this.user = JSON.parse(userData);
            var dat = {};
            dat[data.id] = {};
            dat[data.id]['id'] = data.id;
            dat[data.id]['name'] = data.name;

            if (this.user[data.id] == undefined) {
                var before = userData.toString().substr(1, (userData.toString().length) - 2);
                $.session.set('user_tab', '{' + before + ',"' + data.id + '":{"id": "' + data.id + '","name": "' + data.name + '"}}');
            }
        } else {
            $.session.set('user_tab', '{"' + data.id + '":{"id": "' + data.id + '","name": "' + data.name + '"}}');
        }
    },
// получает массив, если есть, в сессии с открытими далогами
    getMassUser: function getMassUser(id) {
        var userData = $.session.get('user_tab');
        if (userData == '{}') {
            userData = undefined;
        }

        if(userData != undefined) {
            userData = JSON.parse(userData);
        }

        if(userData != undefined && id != 'all') {
            userData = userData[id];
        }

        return userData;
    },

    loadDialog: function loadDialog(id, i) {
        primus.checkUserOnline(id);

        var html, id_dialog, id_from, id_to;
        var userData = $.session.get('user_tab');
        if (userData == '{}') {
            userData = undefined;
        }

        userData = JSON.parse(userData);

        if (userData != undefined) {
            if (userData[id]['id_dialog'] != undefined) {
                id_dialog = userData[id]['id_dialog'];
                $('#usertab-' + id).addClass('dialogtab-' + id_dialog);
                var obj = {global: 'messages',
                    body:{
                        method:'join',
                        param:{
                            action      :'join',
                            room        : id_dialog,
                            id_user     : id,
                            id_dialog   : id_dialog
                        }
                    }
                };

                this.primus.writeSoc(obj);

                obj = {global: 'messages',
                    body:{
                        method:'load_mess',
                        param:{
                            action         :'load_dialogs',
                            room           : id_dialog,
                            id_dialog      : id_dialog,
                            id_user        : id,
                            id_user_from   : this.id_this_user,
                            last_id        : i,
                            limit          : 20
                        }
                    }
                };

                this.primus.writeSoc(obj);
            } else {
                if(i <= 10) {
                    id_from = this.id_this_user;
                    id_to   = id;
                    dn = 'id_from=' + id_from + '&id_to=' + id_to;
                    //data['id_from']+ "/" + id_to
                    $.ajax
                    ({
                        type: "post",
                        url: this.url + "messages/getIdDialog/",
                        data: dn,
                        cache: false,
                        //dataType: "JSON",
                        success: function (data) {
                            if (data[0]['result'] == 'ok') {
                                userData[id]['id_dialog'] = data[0]['id'];
                                userData[id]['email'] = data[0]['email'];
                                userData[id]['role'] = data[0]['role'];
                                userData[id]['first_id'] = data[0]['first_id'];
                                userData[id]['last_id'] = data[0]['last_id'];

                                if(data[0]['avatar']) {
                                    userData[id]['avatar'] = '<img src="/avatars/' + data[0]['avatar'] + '" alt="">';
                                } else {
                                    userData[id]['avatar'] = '<img src="http://placehold.it/300" alt="">';
                                }

                                $.session.set('user_tab', JSON.stringify(userData));
                                $('#usertab-' + id).addClass('dialogtab-' + userData[id]['id_dialog']);
                                message.loadDialog(id , i);
                                i++;
                            }
                        }
                    });
                } else {
                    console.log('error getId from nodeJS')
                }
            }
        }
    },
    showDialog: function showDialog(data) {
        var html = '', obj, dat = [];

        if(data == 0) {
            obj = {global: 'messages',
                body:{
                    method:'load_dialogs',
                    param:{
                        action         :'load_dialogs',
                        id_user_from   : this.id_this_user
                    }
                }
            };

            this.primus.writeSoc(obj);
        } else {
            var dat_m = {}, textF, read, img, cl;

            for(var b = 0; (data.old_mess || {}).length > b; b++) {
                //dat_m.push({
                //    id   : data.old_mess[b]['id_dialog'],
                //    mass : data.old_mess[b]
                //});

                dat_m[data.old_mess[b]['id_dialog']] = data.old_mess[b];
            }

            for(var i = 0; (data.text || {}).length > i; i++) {
                dat = data.text[i];

                if(dat.avatar) {
                    img = '<img src="/avatars/'+ dat.avatar +'" alt="">';
                } else {
                    img = '<img src="http://placehold.it/300" alt="">';
                }

                if(dat_m[dat.id] != undefined) {
                    if(dat_m[dat.id]['id_last'] == this.id_this_user && dat_m[dat.id]['id_last'] == dat.id_user) {
                        if(dat.name.split(' ')[1] == undefined) {
                            textF = dat.name + ': ';
                        } else {
                            textF = dat.name.split(' ')[1] + ': ';
                        }
                        read = 'read_first';
                    } else {
                        textF = 'Вы: ';
                        read = 'read_last';
                    }

                    if(dat_m[dat.id][read] == 0) {
                        cl = 'active';
                    } else {
                        cl = '';
                    }
                } else {
                    cl = '';
                    textF = '';
                    dat_m[dat.id] = [];
                    dat_m[dat.id]['text'] = 'В этом диалоге пока нет сообщений '
                }

                html += '<li class="newDialogUs dialogUs-'+ dat.id_user +'" title="Начать диалог с '+ dat.name +'" onclick="message.newDialogUs({&quot;id&quot;:&quot;'+ dat.id_user +'&quot;, &quot;name&quot;:&quot;'+ dat.name +'&quot;})">' +
                    '<div class="clearfix '+ cl +'">' +
                    '<div class="chat-member col-md-10">' +
                    '<div class="chat-member col-md-1" >' +
                    '<a href="#">'+img+'</a>' +
                    '</div>' +
                    '<div class="chat-member col-md-11" style="margin-left: -20px">' +
                    '<h6>'+ dat.name +' <span class="status status-success"></span> <small>&nbsp; &nbsp;' +
                    this.user_role[dat.role] +
                    '</small></h6>' +
                    '<div class="clear">' + textF + dat_m[dat.id]['text'] + '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="chat-actions col-md-2">' +
                    '<a class="btn btn-link btn-icon btn-xs collapsed" data-toggle="collapse" href="javascript:void(0)"><i class="icon-bubble3"></i></a>' +
                    '</div>' +
                    '</div>' +
                    '</li>';
            }

            if(data.text.length == 0) {
                html += '<div class="alert alert-info fade in">' +
                    '<button type="button" class="close" data-dismiss="alert">×</button>' +
                    '<i class="icon-info"></i> Нет диалогов' +
                    '</div>';
            }

            $('#last_dialog > ul').html(html);

            for(var i = 0; data.text.length > i; i++) {
                primus.checkUserOnline(data.text[i]['id_user']);
            }
        }
    },

    enterMessage: function enterMessage(t) {
        var id_user_to, text, mass, messLoad;
        id_user_to = $(t).closest(".tab-pane").attr('id').split('usertab-')[1];
        text = $(t).closest(".tab-pane").find('.enter-message').val().trim();
        mass = this.getMassUser(id_user_to)


        if(text && mass != undefined) {
            var obj = {global: 'messages',
                body:{
                    method:'enter_mess',
                    param:{
                        room        : mass.id_dialog,
                        id_user     : mass.id,// id to
                        id_user_from: this.id_this_user,// id from
                        text        : text,
                        id_dialog   : mass.id_dialog,
                        img         : '',
                        file        : ''
                    }
                }
            };
            this.primus.writeSoc(obj);

           messLoad = '<div class="message mess-load">' +
                '<a class="message-img" href="#"><img src="http://placehold.it/300" alt=""></a>' +
                '<div class="message-body">' +
                    '<span class="typing"></span>' +
                '</div>' +
            '</div>';
            $(t).closest('.panel-collapse').find('.enterMessage').addClass('disabled');
            $(t).closest('.panel-collapse').find('.chat > div').append(messLoad);
            $(t).closest(".tab-pane").find('.enter-message').val('')
        }
    },

    messUp: function messUp(param) {
        var mess = '', cl, patch, dat, length, img, mass, name, main, clm = '', read = 0, data = [];

        if(typeof param.text == 'object') {
            length = param.text.length - 1;
        } else {
            length = 0;
        }
            for(var i = length; i >= 0; i--) {
                if(length == 0 && typeof param.text != 'object') {
                    dat = param;
                } else {
                    dat = param.text[i];
                }

                if(dat.id_first != this.id_this_user) {
                    mass = this.getMassUser(dat.id_first);
                    cl = '';
                    img = mass.avatar;
                    name = mass.name;
                    main = 'read_last';
                } else {
                    mass = this.getMassUser(dat.id_last);

                    cl = 'reversed';
                    img = this.avatar_user;
                    name = this.name_user;
                    main = 'read_first';
                }

                switch (dat[main]) {
                    case 0:
                        clm = 'noread';
                        read = 1;
                        data.id_dialog = dat.id_dialog;
                        this.readMess(data, 0);
                        break;
                    case 1:
                        clm = '';
                        break;
                    case 2:
                        clm = 'error';
                        break;
                }

            mess += '<div class="message '+ cl +' message_'+ dat.id +'">' +
                '<a class="message-img" href="#">' +img+ '</a>' +
                '<div class="message-body '+ clm +'">' +
                '<span class="attribution">'+ name +' '+ dat.time.split('.')[0] +'</span>' +
                dat.text +
                '</div>' +
                '</div>';
            }

        //if(read == 1) {
        //    data.id_dialog = dat.id_dialog;
        //    data.main = main;
        //    this.readMess(data, 0);
        //}

        if(length == -1) {
            mess = '<br /><div class="alert alert-info fade in">' +
                '<button type="button" class="close" data-dismiss="alert">×</button>' +
                '<i class="icon-info"></i> Нет сообщений' +
                '</div><br />';
        }


        if((length == 0 && typeof param.text != 'object') || length == -1) {
            patch = '.dialogtab-'+ param.id_dialog +' .panel-collapse > .chat';
            $('.dialogtab-'+ param.id_dialog +' .panel-collapse > .enter-message').focus();
            $('.enterMessage').removeClass('disabled');
            $(patch + ' > div > .mess-load').remove();
        } else {
            dat = param.text[0];
            patch = '.dialogtab-'+ dat.id_dialog +' .panel-collapse > .chat';
        }

        $(patch + ' > div').append(mess);
        $(patch + ' > div > .clear_m').remove();
        $(patch + ' > div').append('<div class="clear_m"></div>');

        setTimeout(function() {
            $(patch).scrollTop($(patch + ' > div').height());
        }, 300);
    },

// readMess читаем новые сообщения
    readMess: function readMess(data, t) {
        var f;

        //alert('')
        if(t == 0) {
            if(data.main = 'read_first') {
                data.where = 'id_last';
            } else {
                data.where = 'id_first';
            }

            var obj = {global: 'messages',
                body:{
                    method:'read_mess_dialog',
                    param:{
                        id_user_from: this.id_this_user,// id from
                        id_dialog   : data.id_dialog,
                        main        : 'read_last',
                        where       : 'id_last'
                    }
                }
            };
            this.primus.writeSoc(obj);


            var obj1 = {global: 'messages',
                body:{
                    method:'read_mess_dialog',
                    param:{
                        id_user_from: this.id_this_user,// id from
                        id_dialog   : data.id_dialog,
                        main        : 'read_first',
                        where       : 'id_first'
                    }
                }
            };

            this.primus.writeSoc(obj1);
        } else {
            $('.mess-noread-h').html(parseInt($('.mess-noread-h').html()) - data.text.changedRows);
            $('.usertab-'+ data.id_user +'li  > a > .badge').html('');
        }

    },

// getInfoUser
    getInfoUser: function getInfoUser(id) {
        // if (id != undefined) {
            return $.ajax
            ({
                type: "post",
                url: this.url + "messages/getInfoUser/" + id,
                data: 'id_user=' + id,
                cache: false,
                method: 'GET',
                async: false,
                dataType: "JSON",
                success: function (data) {
                    if (data[0]['result'] == 'ok') {
                        return data[0];
                    } else {
                        return false;
                    }
                }
            });
        // } else {
        //     return false;
        // }
    },

// всплывающие уведомление + оповещение о новом сообщении в открытом  неактивном диалоге
// newMessage
    newMessage: function newMessage(param) {
        var f;
        message.showDialog(0);

        f = $('.mess-noread-h').html();

        mass = this.getInfoUser(param.id_user_from);
        mass_open = this.getMassUser(param.id_user_from);

        if(!$('.usertab-' + param.id_user_from + 'li').hasClass('active')) {
            mass.success(function(realData) {
                realData[0]['main'] = 'read_last';
                message.setMesWin(mass_open, realData[0], param.text);
                $('.mess-noread-h').html(parseInt($('.mess-noread-h').html()) + 1);
            });
        }
    },

// show min win
    setMesWin: function setMesWin(mass_open, mass, text) {
        if(mass.img) {
            img = '<img src="/avatars/' + mass.img + '" alt="">';
        } else {
            img = '<img src="http://placehold.it/300" alt="">';
        }

        var openTab;
        if($('.usertab-'+ mass.id +'li').hasClass('active')
            &&
            $('#see_dialog_li').hasClass('active')) {
            openTab = true;
        } else {
            openTab = false;

            var parser = document.createElement('a');
            parser.href = document.location.href;

            if(parser.pathname.indexOf('messages') + 1) {
                var n = $('.usertab-'+ mass.id +'li > a > .badge').html();
                if(!n) {n = 0;}
                $('.usertab-'+ mass.id +'li  > a > .badge').html(parseInt(n) + 1);
            }
        }

        if(mass_open == undefined || openTab == false) {
            var mess_push = '' +
                '<a class="message-img mess_push" href="#" onclick="message.newDialogUs({\'id\':\''+mass.id+'\', \'name\':\''+mass.name+'\', \'check\':\'true\'})">' + img + '</a>' +
                mass.name + ' <br/>' +
                '<a class="message-img mess_push" href="#" onclick="message.newDialogUs({\'id\':\''+mass.id+'\', \'name\':\''+mass.name+'\', \'check\':\'true\'})">' +
                text +
                '</a>' +
                '<div class="clear"></div>';
            $.jGrowl(mess_push, { sticky: false, header: 'Новое сообщение ' });
        }
    },
    /**/ /*оповещения*/ /**/
    noticesMessage: function noticesMessage(param)
    {
       return $.ajax
        ({
            url: "/notices/send_alert_mess?_token=" + this.csrf_token,
            data: param,
            cache: false,
            type: 'POST',
            async: false,
            dataType: "JSON",
            success: function (data) {
                return data['result'];
            }
        });
    }
    /**/ /*оповещения*/ /**/
};
