var primus = {
    idUser: undefined,
    primusCon: new Primus('ws://91.240.87.136:8013/', {}),
    primusInit: function primusInit(s) {
        this.idUser = s['idUser'];

        this.primusCon.end().open();

        this.primusCon.on('outgoing::url', function connectionURL(url) {
            url.query = 'token=' + (localStorage.getItem('token') || '');
        });

        this.primusCon.on('outgoing::url', function connectionURL(url) {
            url.query = 'token=' + (localStorage.getItem('token') || '');
        });

        this.primusCon.on('close', function close() {
            status.textContent = 'disconnected';
        });

        this.primusCon.on('data', function received(data) {primus.rouSoc(data);});

        /*/ update time online /*/
        var set_time;
        var setUp_time = 300;
        var time = new Date().getTime();
        var old_time = $.session.get('user_' + this.idUser);

        if(old_time == undefined) {
            set_time = 1000;
        } else {
            set_time = (time - old_time) / 1000;
        }
        //alert(user)
        if(set_time > setUp_time) {
            primus.setOnlStU(this.idUser);
            $.session.set('user_' + this.idUser, time);

        }

        updareTimeUesr(this.idUser);

        function updareTimeUesr(user) {
            setTimeout(function() {
                primus.setOnlStU(user);
                $.session.set('user_' + user, new Date().getTime());
                updareTimeUesr(user);


            }, setUp_time * 1000);
        }
        /*/ update time online /*/

        primus.getNMess(this.idUser);

        var data = [];
        data.room_us = 'm'+this.idUser;
        primus.joinRoom(data);
    },
// определение цвета активности
    rouSoc: function rouSoc(data)
    {
        switch (data.global) {
            case 'user':
                switch (data.body.method) {
                    case 'get_user_online':
                        if(data.body.param.time > 0) {
                            $('.dialogUs-'+ data.body.param.user +' > .clearfix > .chat-member > div > h6 > span').attr('class', 'status status-success');
                            $('.dialogUs-'+ data.body.param.user +' > .clearfix > .chat-member > h6 > span').attr('class', 'status status-success');
                            $('.usertab-'+ data.body.param.user +'li > a > span.status').attr('class', 'status status-success');
                        }else {
                            $('.dialogUs-'+ data.body.param.user +' > .clearfix > .chat-member > div > h6 > span').attr('class', 'status status-default');
                            $('.dialogUs-'+ data.body.param.user +' > .clearfix > .chat-member > h6 > span').attr('class', 'status status-default');
                            $('.usertab-'+ data.body.param.user +'li > a > span.status').attr('class', 'status status-default');
                        }
                        break;
                }
                break;
            case 'messages':
                switch (data.body.method) {
                    case 'get_noread_messages':
                        this.counterMess(data.body.param);
                        break;
                    case 'join_room':
                        break;
                    case 'new_message':
                        //alert(message.newMessage(data.body.param))
                        message.rouMess(data.body);
                        break;
                    default:
                        message.rouMess(data.body);
                        break;
                }
                break;
        }
    },
// send obj socs
    writeSoc: function writeSoc(obj)
    {
        this.primusCon.write(obj)
    },
// add user online
    setOnlStU: function setOnlStU(id) {
        this.writeSoc({global: 'user', body: {method: 'set_user_online', param: {id: id}}});
    },
// проверка юзера на онлайн
    checkUserOnline: function checkUserOnline(id) {
        this.writeSoc({global: 'user', body: {method: 'get_user_online', param: {id: id}}})
    },
// проверка непрочитанных сообщений count
    getNMess: function getNMess(id) {
        this.primusCon.write({global: 'messages',body:{method:'get_noread_messages', param:{id_user_from:id}}})
    },
// counterMess
    counterMess: function counterMess(param) {
        //alert(print_r(param.text[0], true))

        $('.mess-noread-h').html(param.text[0]['count_nodead']);
    },
// joinRoom
    joinRoom: function joinRoom(data) {
        //alert(data.room)
        this.primusCon.write({global: 'messages',body:{method:'join_room', param:{room_us:data.room_us}}})
    }
};