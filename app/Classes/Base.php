<?php

namespace App\Classes;

use App\Activity;
use App\Task;
use App\Userlist;
use Auth;
use Illuminate\Foundation\Auth\User;
use Mail;
use Session;
use App\Classes\DynamicModel;

class Base
{
    // путь для загрузки присоединённых файлов
    const UPLOAD_PATH = '/public/uploads/';
    // путь для аватаров
    const AVATAR_PATH = '/public/avatars/';
    // основной урл сайта
    const BASE_URL = 'http://task.niws.ru/';

    /**
     * @param array $r
     * @param $project_id
     * @param bool $a
     * @return bool
     */
    static function project_role($r = [], $project_id, $a = false) {
        $project_role = Userlist::where(['projectId' => $project_id, 'userId' => Base::$user->id])->first();

        if(Base::$user->role == 'SA') $project_role['role'] = 'A';
        if($r == []) return !$a ? $project_role['role'] : $project_role;

        return array_search($project_role['role'], $r) === false ? false : true;
    }

    /**
     * проверка разрешения редактировать одного пользователя относитлеьно другого
     * разрешено редактировать супер админу и администратору который создал профиль пользователя
     * @param $id_two_user
     * @param null $id
     * @return bool
     */
    static function user_role($id_two_user, $id = null) {
        $two_user = User::where(['id' => $id_two_user, 'id_user_create' => $id ?? Base::$user->id])->first();

        if($id) {
            $user = User::where(['id' => $id])->first();
            $right = $user['role'] === 'SA' || ($two_user && $user['role'] === 'A');
        } else {
            $right = Base::$user->role === 'SA' || ($two_user && Base::$user->role === 'A');
        }

        return $right;
    }

    // роли в системе
    public static $projects_roles = [
        'SA' => 'SYS администратором',
        'A' => 'администратором',
        'M' => 'менеджером',
        'U' => 'пользователем'
    ];

    // poles in system
    public static $user_role = [
        'SA' => 'SYS Администратор',
        'A' => 'Администратор',
        'M' => 'Менеджер',
        'U' => 'Пользователь'
    ];

    // statuses
    public static $statuses = [
        'active' => ['inwork', 'test', 'review', 'makeup'],
        'closed' => ['closed', 'paused', false]
    ];

    // statuses text
    public static $statuses_text = [
        'inwork' => 'В работе',
        'test'   => 'На тестировании',
        'review' => 'В предосмотре',
        'makeup' => 'В работе у дизайнера',
        'closed' => 'Закрытая',
        'paused' => 'Приостановлена',
        '' => 'Без статуса'
    ];

    // statuses text
    public static $statuses_color = [
        'inwork' => 'inwork',
        'test' => 'warning',
        'review' => 'warning',
        'makeup' => 'warning',
        'closed' => 'default',
        'paused' => 'default',
        '' => 'default'
    ];

    // right action in system
    public static $action_projects_roles = [
        'SA' => 'всё u GOD',
        'A' => 'добавить, просмотреть, редактировать, завершить или удалить проекты',
        'M' => 'просмотривать, редактировать или завершить проекты',
        'U' => 'просмотривать проекты'
    ];

    // роли в просмотре проекта
    public static $project_roles = [
        'администратором',
        'менеджером',
        'пользователем'
    ];

    // описание действий в просмотре проекта
    public static $actionProjecRoles = [
        'удалить проект, добавлять и удалять пользователей в проект, добавлять, закрывать и удалять задачи, добавлять и удалять пользователей в задчи',
        'добавлять и удалять пользователей в проект, добавлять и закрывать задачи, добавлять и удалять пользователей в задачах',
        'просматривать назначенные вам задачи',
    ];

    // массив стилей для иконок активности пользователей
    public static $activity_actions = [
        'create' => 'icon-plus-circle2 text-success',
        'edit' => 'icon-pencil text-info',
        'delete' => 'icon-remove2 text-danger'
    ];

    // массив статусов задач, name-название статуса, css-класс css для вывода
    public static $task_status = [
        'new' => [
            'name' => 'Новая',
            'css' => 'danger'
        ],
        'inwork' => [
            'name' => 'В работе',
            'css' => 'inwork'
        ],
        'closed' => [
            'name' => 'Закрыта',
            'css' => 'default'
        ],
        'reopen' => [
            'name' => 'Переоткрыта',
            'css' => 'warning'
        ],
        'reading' => [
            'name' => 'Просмотрена',
            'css' => 'success'
        ],
        'test' => [
            'name' => 'На тестировании',
            'css' => 'warning'
        ],
        'update' => [
            'name' => 'Обновлена',
            'css' => 'updated'
        ],
        '' => [
            'name' => 'без статуса',
            'css' => 'success'
        ],
    ];

    // массив приоритетов задач, name-название приоритета, css-класс css для вывода
    public static $task_priority = [
        'standard' => [
            'name' => 'Обычный',
            'css' => 'success'
        ],
        'low' => [
            'name' => 'Низкий',
            'css' => 'low'
        ],
        'high' => [
            'name' => 'Высокий',
            'css' => 'danger'
        ]
    ];

    // чёрный список расширений для загружаемых файлов
    public static $blackExt = [
        'php', 'html', 'js', 'phtml', 'php3', 'php4', 'htm'
    ];

    // список расширений для графических файлов
    public static $imgExt = [
        'jpg',
        'jpeg',
        'bmp',
        'png',
        'gif'
    ];

    // список mime типов для графических файлов
    public static $imgMime = [
        'image/jpg',
        'image/bmp',
        'image/png',
        'image/gif',
        'image/jpeg'
    ];

    // авторизованный пользователь
    public static $user;

    /**
     * инициация авторизированного пользователя, будет доступер везде в Base::$user
     */
    static public function init()
    {
        self::$user = Auth::user();
    }

    /**
     * редирект назад с сообщением об ошибке
     * @param $message - сообщение об ошике
     * @return \Illuminate\Http\RedirectResponse - возвращаем назад
     */
    public static function wrong($message)
    {
        Session::flash('error', $message);

        return back()->withInput();
    }

    /**
     * редирект назад с сообщением
     * @param $message - сообщение
     * @return \Illuminate\Http\RedirectResponse - возвращаем назад
     */
    public static function back($message)
    {
        Session::flash('info', $message);

        return redirect()->back();
    }

    /**
     * редирект с сообщением
     * @param $url - путь редиректа
     * @param $message - сообщение
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector редирект на $url
     */
    public static function redirect($url, $message)
    {
        Session::flash('info', $message);

        return redirect($url);
    }

    /**
     * формируем вьюху, с обязательными параметрами
     * @param $url - путь вьюхи
     * @param array $args - аргументы
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View возвращаем вьюху
     */
    public static function view($url, $args = [])
    {
        if (Auth::check()) {
            if (self::$user->role == 'A') self::$user->roleName = "Администратор";
            elseif (self::$user->role == 'M') self::$user->roleName = "Менеджер";
            else self::$user->roleName = 'Пользователь';

            $args['my_tasks'] = Task::where(['userAssigned' => self::$user->id])
                ->where('processes', '!=', 'closed')
                ->orderBy('id', 'DESC')
                ->get();

            $args['Activities'] = Activity::where('created_at', '>=', date('Y-m-d') . " 00:00:00")
                ->orderBy('created_at', 'desc')
                ->get();

            $args['project_count'] = Userlist::where('userId', self::$user->id)->groupBy('projectId')->count();
            $args['high_tasks'] = 0;

            foreach ($args['my_tasks'] as $v) {
                if($v->priority == 'high') {
                    $args['high_tasks'] = ++$args['high_tasks'];
                }
            }

            $args['all_users'] = User::get();
            $args['User'] = self::$user;
            $args['user_role'] = self::$user_role;
            $args['task_priority'] = Base::$task_priority;

            return view($url, $args);
        } else {
            echo "<h3>Вы не зарегестрированный пользователь, <a href='" . self::BASE_URL . "/login'>зарегестрируйтесь пожалуйста</a>!</h3>";

            return;
        }
    }

    /**
     * формируем склонения
     * @param $number - число, для которого формируем
     * @param $after - массив склонений, например ['день','дня','дней']
     * @return string - возвращаем склонение
     */
    public static function plural($number, $after)
    {
        $cases = array(2, 0, 1, 1, 1, 2);
        return $number . ' ' . $after[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
    }

    /**
     * записываем активность пользователя
     * @param $message - сообщение активонсти
     * @param $message
     * @param $action
     * @param $id_project
     */
    public static function activity($message, $action, $id_project)
    {
        $Activity = new Activity();

        $Activity->userId = self::$user->id;
        $Activity->text = "Пользователь <a href='#'>" . self::$user->name . '</a> ' . $message;
        $Activity->action = $action;
        $Activity->id_project = $id_project;

        $Activity->save();
    }

    /**
     * Отправляет письмо
     * @param $layout - шаблон письма
     * @param $user - модель пользователя
     * @param $password - пароль пользователя
     */
    public static function mail($layout, $user, $password)
    {
        $user->base_user = Base::BASE_URL;

        Mail::send('emails.' . $layout, ['user' => $user, 'password' => $password], function ($m) use ($user) {
            $m->from('taskmanager@mail.ru', 'Служба техподдержки');
            $m->to($user->email, $user->name)->subject('Создание аккаунта');
        });
    }

    /**
     * Поиск пользователя по id
     * @param $id - пользователя
     * @return bool|null - false - если пустой или не валидный id, null - если пользователь не найден, модель пользователя, если всё ок
     */
    public function getUserById($id)
    {
        if (empty($id) || (int)$id < 1) return false;
        $User = User::find($id);
        if ($User == null) return null;

        return $User;
    }

    /**
     * функция для получения массива id и массива проектов в которых состоит пользователь
     * @param null $user_id
     * @return mixed
     */
    public static function projectsAccessByUserId($user_id = null)
    {
        $Mod = new DynamicModel();

        if(!$user_id) {
            $user_role = Base::$user->role;
        } else {
            $user = $Mod->t('users')->where('id', $user_id)->first();
            $user_role = $user['role'];
        }

        if($user_role == 'SA') {
            $data['projects'] = $Mod->t('projects')->get();
        } else {
            $data['projects']  = $Mod
                ->t('projects')

                ->join('userlist', function($join)
                {
                    $join->type = 'INNER';
                    $join->on('projects.id', '=', 'userlist.projectId');
                })

                ->where('userlist.userId', '=', $user_id)
                ->select('projects.*', 'userlist.role')
                ->groupBy('projects.id')
                ->orderBy('projects.created_at', 'desc')
                ->get();
        }

        $projects_access = [];
        foreach ($data['projects'] as  $v) {
            $projects_access[] = $v->id;
        }

        $data['id'] = empty($projects_access) ? [0] : $projects_access;

        return $data;
    }
}
