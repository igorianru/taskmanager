<?php
namespace App\Classes;

use Illuminate\Support\Facades\DB;
use Schema;
use Request;
use Eloquent;

class DynamicModel extends  Eloquent
{
    /**
     * @var
     */
    protected static $_table;

    /**
     * @param $table
     * @param array $parms
     * @return null|static
     */
    public static function t($table, $parms = Array()){
        $ret = null;

        if (class_exists($table))
        {
            $ret = new $table($parms);
        } else {
            $ret = new static($parms);
            $ret->setTable($table);
        }
        return $ret;
    }

    /**
     * ->select($Mod::raw('SUM(size) as size'), $Mod::raw('COUNT(id) as count'))
     * @param $raw
     * @return mixed
     */
    public static function raw($raw){
        return DB::raw($raw);
    }

    /**
     * @param $table
     */
    public function setTable($table)
    {
        static::$_table = $table;
    }

    /**
     * @return mixed
     */
    public function getTable()
    {
        return static::$_table;
    }

    /**
     * @return mixed
     */
    public function getAllTableName()
    {
        $table = DB::select('SHOW TABLES');

        foreach ($table as $key => $val) {
            $val = (array) $val;
            $arr[] = array_shift($val);
        }

        return $arr;
    }


    /**
     * @param null $name
     * @return array
     */
    public function getAllColumnTableName($name = null)
    {
        if($name) {
            $table = DB::select('SHOW FIELDS FROM ' . $name);

            foreach ($table as $key => $val) {
                $val = (array) $val;
                $arr[] = array_shift($val);
            }

            return $arr;
        } else {
            return [];
        }
    }
}