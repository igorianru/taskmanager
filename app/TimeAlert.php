<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeAlert extends Model
{
    protected $table = 'time_alert';
}
