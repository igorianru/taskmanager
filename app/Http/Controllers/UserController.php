<?php
namespace App\Http\Controllers;

use App\Classes\Base;
use App\Classes\SimpleImage;
use App\Task;
use App\TimeAlert;
use App\User;
use App\Userlist;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Response;

class UserController extends Controller
{

    public function __construct(Request $request)
    {
        parent::__construct();
        $this->request = $request->all();
    }

    public function profile_redirect()
    {
        return redirect('/user/profile/' . Base::$user->id);
    }
    public function index()
    {
        try {
            if (Base::$user->role !== 'SA') return Base::view('errors/404', ['debug' => false]);

            $data['users'] = User::paginate(10);
            $data['pagination'] = ''.$data['users']->render();

            return Base::view('users.all_user', $data);
        } catch (\Exception $err) {
            return Base::view('errors/error', ['debug' => false]);
        }
    }

    /**
     * страница редактирования профиля
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function profile_edit($id = null)
    {
        try {
            if(isset($this->request['name'])) {
                if (empty($this->request['name'])) return Base::wrong("Не заполнено имя пользователя!");
                if (empty($this->request['email'])) return Base::wrong("Не заполнен email!");

                if($this->request['confirm_password'] != '') {
                    if ($this->request['password'] != $this->request['confirm_password'])
                        return Base::wrong("Не совпадают пароль и проверочный пароль!");
                }

                $User = User::find(Base::$user->id);

                if ($User == null) {
                    Auth::logout();
                    return redirect('/');
                }

                if (!empty($this->request['avatar'])) {
                    $avatar = $this->request['avatar'];
                    $ext = $avatar->getClientOriginalExtension();
                    if (in_array($ext, Base::$blackExt)) return Base::wrong('Не допустимый тип файла!');

                    if (!in_array(strtolower($avatar->getClientOriginalExtension()), Base::$imgExt) ||
                        !in_array($this->request['avatar']->getMimeType(), Base::$imgMime)
                    )
                        return Base::wrong('Аватар не является графическим файлом!');

                    $fileName = str_random(10) . rand(0, microtime(true)) . str_random(5) . '.' . $ext;
                    $avatar->move(base_path() . Base::AVATAR_PATH, $fileName);
                    chmod(base_path() . Base::AVATAR_PATH . $fileName, 0777);

                    if (empty($User->avatar)) {
                        $User->avatar = $fileName;
                    } else {
                        $unlink = @unlink(base_path() . Base::AVATAR_PATH . $User->avatar);
                        $User->avatar = $fileName;
                    }

                    $image = new SimpleImage();

                    $image->load(base_path() . Base::AVATAR_PATH . $fileName);
                    $image->resizeToWidth(300);
                    $image->save(base_path() . Base::AVATAR_PATH . $fileName);
                }

                $User->name               = $this->request['name'];
                $User->email              = $this->request['email'];
                $User->time_alert_message = $this->request['time_alert_message'];
                $User->time_alert_comment = $this->request['time_alert_comment'];
                $User->nickname           = $this->request['nickname'];
                $User->role_in_team       = $this->request['role_in_team'];

                if (!empty($this->request['password'])) {
                    $User->password = Hash::make($this->request['password'] . LoginController::SALT);
                }

                $User->save();

                return Base::redirect("/user/profile/$id", "Данные успешно изменены!");
            } else {
                if (!Base::user_role($id) && $id != Base::$user->id)
                    return Base::wrong('У вас нет прав для редактирования данного пользователя.');

                $TimeAlert          = new TimeAlert();
                $data['alert_time'] = $TimeAlert::all();
                $data['user']       = User::where('id', $id)->first();

                return Base::view('users.profile_edit', $data);
            }
        } catch (\Exception $err) {
            return Base::view('errors/error', ['debug' => false]);
        }
    }

    /**
     * вывод страницы профиля
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile_id($id = null)
    {
        $tasks                  = new Task();
        $TimeAlert              = new TimeAlert();
        $data['alert_time']     = $TimeAlert::all();
        $data['user']           = User::where('id', $id)->first();
        $data['user_role']      = Base::$user->role;
        $data['user_role_name'] = Base::$projects_roles[$data['user_role']];
        $data['user_role_text'] = Base::$action_projects_roles[$data['user_role']];
        $data['statuses_text']  = Base::$statuses_text;
        $data['statuses_color'] = Base::$statuses_color;
        $projects_access        = Base::projectsAccessByUserId($id);
        $id_projects            = $projects_access['id'];
        $data['projects_count'] = $id_projects !== [0] ? count($id_projects) : 0;
        $data['projects']       = $projects_access['projects'];

        if(empty($data['user'])) return Base::view('errors/404', ['debug' => false]);

        $data['tasks_open'] = $tasks
            ->join('users', function($join)
            {
                $join->type = 'LEFT OUTER';
                $join->on('tasks.userAssigned', '=', 'users.id');
            })

            ->join('projects', function($join) use ($id_projects)
            {
                $join->type = 'RIGHT OUTER';

                $join
                    ->on('tasks.projectId', '=', 'projects.id')
                    ->whereIn('projects.id', $id_projects);
            })

            ->where('tasks.processes', '!=', 'closed')
            ->where('tasks.userAssigned', $id)
            ->select('users.name as user_name', 'tasks.*', 'projects.name as projects_name')
            ->groupBy('tasks.id')
            ->orderBy('tasks.created_at', 'desc')
            ->limit(200)
            ->get();

        $data['tasks_active'] = $tasks

            ->join('users', function($join)
            {
                $join->type = 'RIGHT OUTER';
                $join->on('tasks.userAssigned', '=', 'users.id');
            })

            ->join('projects', function($join) use ($id_projects)
            {
                $join->type = 'RIGHT OUTER';

                $join
                    ->on('tasks.projectId', '=', 'projects.id')
                    ->whereIn('projects.id', $id_projects);
            })

            ->whereIN('tasks.processes', Base::$statuses['active'])
            ->where('tasks.userAssigned', $id)
            ->select('users.name as user_name', 'tasks.*', 'projects.name as projects_name')
            ->groupBy('tasks.id')
            ->orderBy('tasks.created_at', 'desc')
            ->limit(200)
            ->get();

        $data['tasks_closed'] = $tasks

            ->join('users', function($join)
            {
                $join->type = 'LEFT OUTER';
                $join->on('tasks.userAssigned', '=', 'users.id');
            })

            ->join('projects', function($join) use ($id_projects)
            {
                $join->type = 'RIGHT OUTER';

                $join
                    ->on('tasks.projectId', '=', 'projects.id')
                    ->whereIn('projects.id', $id_projects);
            })

            ->where('tasks.processes', '=', 'closed')
            ->where('tasks.userAssigned', $id)
            ->select('users.name as user_name', 'tasks.*', 'projects.name as projects_name')
            ->groupBy('tasks.id')
            ->orderBy('tasks.created_at', 'desc')
            ->limit(200)
            ->get();

        return Base::view('users.profile_id', $data);
    }

    /**
     * Страница добавления пользователя
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewAdd()
    {
        try {
            return Base::view('users.add');
        } catch (\Exception $err) {
            return Base::view('errors/error', ['debug' => false]);
        }
    }

    /**
     * Страница удаления пользователя
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewDelete()
    {
        try {
            $allUsers = User::all();
            return Base::view('users.delete', [
                'allUsers' => $allUsers
            ]);
        } catch (\Exception $err) {
            return Base::view('errors/error', ['debug' => false]);
        }
    }

    /**
     * Добавление пользователя в систему
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View|Response
     */
    public function add()
    {
        try {
            if (Base::$user->role != 'A') return Base::wrong('Вы не можете добавить пользователя!');
            if (empty($this->request['name']) || empty($this->request['email'])
                || empty($this->request['password']) || empty($this->request['role'])
            ) return Base::wrong('Одно из полей не заполнено!');

            $User = new User();

            $User->name = $this->request['name'];
            $User->email = $this->request['email'];
            $User->role = $this->request['role'];
            $User->password = Hash::make($this->request['password'] . LoginController::SALT);

            $User->save();

            Base::mail('addUser', $User, $this->request['password']);

            return Base::back("Пользователь $User->name успешно добавлен!");
        } catch (\Exception $err) {
            return Base::view('errors/error', ['debug' => false]);
        }
    }

    /**
     * Удаление пользователя из системы
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function delete()
    {
        try {
            if (Base::$user->role != 'A') return Base::wrong('Вы не можете удалить пользователя!');
            if ($this->request['user'] == 1 || $this->request['user'] == 2) return Base::wrong("Нельзя удалить этого пользователя, отсоси!!!");
            if (empty($this->request['user'])) return Base::wrong('Нет id пользователя!');
            if ((int)$this->request['user'] < 1) return Base::wrong("Нет такого пользователя!");

            $User = User::find($this->request['user']);

            if ($User == null) return Base::wrong("Нет такого пользователя!");

            $name = $User->name;
            Userlist::where('userId', $User->id)->delete();

            $User->delete();

            return Base::back("Пользователь $name успешно удалён");
        } catch (\Exception $err) {
            return Base::view('errors/error', ['debug' => false]);
        }
    }

}
