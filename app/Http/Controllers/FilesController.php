<?php

namespace App\Http\Controllers;

use App\Classes\Base;
use App\Classes\DynamicModel;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Validator;

class FilesController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request  = $request->all();
        $this->requests = $request;
        $this->dynamic  = new DynamicModel();
    }

    /**
     * function load files
     * @param null $data
     * @return array
     */
    public function load_files($data = null)
    {
        try {
            if(!$data) {
                $request = $this->requests;

                $data['file'][0]      = $request->file("Filedata");
                $data['mime_type'][0] =$data['file'][0]->getMimeType();
                $data['id_task']      = $request->input("id_task");
                $data['id_project']   = $request->input("id_project");
                $data['small_width']  = 400;
                $data['small_height'] = 400;
            }

            $img_mime_type = [
                'image/jpg',
                'image/gif',
                'image/GIF',
                'image/png',
                'image/jpeg',
                'image/JPG',
                'image/PNG',
                'image/JPEG',
            ];

            if(array_search($data['mime_type'][0], $img_mime_type)) {
                $data['type'] = $res['type'] = 'images';
                $res['data']  = $this->_upload_img($data);

            } else {
                $data['type'] = $res['type'] = 'file';
                $res          = $this->_upload_file($data);
            }

            $res['result'] = 'ok';

            return json_encode($res);
        } catch (\Exception $err){
            return Base::view('errors.error', ['debug'=>false]);
        }
    }

    public function get_files()
    {
        try {
            $Mod                 = $this->dynamic;
            $request             = $this->requests;
            $data['id_task']     = $request->input("id_task");
            $data['id_project']  = $request->input("id_project");
            $data['currentFile'] = $request->input("currentFile");
            $data['attachment']  = $request->input("attachment");
            $data['search']      = $request->input("search");

            $where = [];

            if($data['search']['project'] == 'this')
                $where['id_project'] = $data['id_project'];

            if($data['search']['users'] == 'this')
                $where['user_id'] = Base::$user->id;

             if($data['id_task'] != '' && $data['id_project'] != '' ) {
                 if(!$data['currentFile']) {
                     $res['data'] = $Mod
                         ->t('files')
                         ->where($where)
                         ->where('orig_name', 'like', '%' . $data['search']['search'] . '%')
                         ->orderBy('id', 'DESC')
                         ->paginate(12);

                     $res['pagination'] = (string) $res['data']->render();

                     $res['files_info'] = $Mod
                         ->t('files')
                         ->where(['id_project' => $data['id_project'], 'user_id' => Base::$user->id])
                         ->select($Mod::raw('SUM(size) as size'), $Mod::raw('COUNT(id) as count'))
                         ->first();
                 } else {
                     $res['data'] = [];
                     $res['data']['data'] = $Mod
                         ->t('files')
                         ->whereIn('id', $data['attachment'])
                         ->get()
                         ->toArray(12);

                     $res['pagination'] = '';
                 }

                 $res['result'] = 'ok';
             } else {
                 $res['text_error'] = 'unknown params';
                 $res['result'] = 'error';
             }

            return json_encode($res);
        } catch (\Exception $err){
            return Base::view('errors.error', ['debug' => false]);
        }
    }

    /**
     * upload images
     * @param $data
     * @return mixed
     */
    private function _upload_img($data) {
        $req        = $res = [];
        $res['img'] = [];
        $valid      = false;
        $file       = $data['file'];
        $mime_type  = is_array($data['mime_type']) ? $data['mime_type'][0] : $data['mime_type'];
        $id_task    = $data['id_task'] ? $data['id_task'] : 0;
        $id_project = $data['id_project'] ?  $data['id_project'] : 0;
        $name       = isset($data['name']) ? $data['name'] : '';
        $type       = $data['type'];
        $sw         = $data['small_width'];
        $sh         = $data['small_height'];

        if(!isset($data['type'])) $data['type'] = 'input';

        foreach ($file as $i => $f) {
            $Mod = $this->dynamic;

            if ($f == null) {
                $valid = true;
            } else {
                if($data['type'] != 'url')
                {
                    $validator2 = Validator::make(
                        ['img' => $f],
                        ['img' => 'image']
                    );

                    if ($validator2->fails())
                    {
                        $valid = true;
                        $res['error'] = "Файл должен быть изображением";
                        break;
                    }
                }

                $path_o = public_path() . "/images/files/original/";
                $path_s = public_path() . "/images/files/small/";

                if($data['type'] == 'url')
                {
                    $fileExt   = $data['ext'];
                    $orig_name = $data['orig_name'];
                    $size      = $data['size'];
                } else {
                    $fileExt   = strtolower($f->getClientOriginalExtension());
                    $orig_name = $f->getClientOriginalName();
                    $size      = $f->getSize();
                }

                $filename = str_random(10) . time() . str_random(5);

                $img = Image::make($f);
                $img->backup();

                /* original */
                $img->save($path_o . $filename .'.'. $fileExt);
                /* original */

                /*  small */
                $img->reset();
                // Crop and resize combined
                $img->fit($sw, $sh);
                $img->save($path_s . $filename .'.'. $fileExt);
                $img->reset();
                /*  small */

                // задаём полные права
                chmod($path_o . $filename .'.'. $fileExt, 0777);
                chmod($path_s . $filename .'.'. $fileExt, 0777);

                if($data['type'] == 'url')
                {
                    unlink($f);
                }

                $file = $Mod->t('files')->where(['id_task' => $id_task, 'id_project' => $id_project])->first();

                $req['type']       = $type;
                $req['name']       = $name;
                $req['mime_type']  = $mime_type;
                $req['id_task']    = $id_task;
                $req['id_project'] = $id_project;
                $req['orig_name']  = $orig_name;
                $req['size']       = $size;
                $req['file']       = $filename.'.'.$fileExt;
                $req['user_id']    = Base::$user->id;
                $req['created_at'] = Carbon::now();

                if(empty($file)) $req['main'] = 1; else $req['main'] = 0;

                $req['id'] = $Mod->t('files')->insertGetId($req);
            }

            $res['img'][] = $req;
        }

        $res['valid'] = $valid;

        return $res;
    }

    public function _upload_file($data)
    {
        $file       = $data['file'];
        $Mod        = $this->dynamic;
        $type       = $data['type'];
        $mime_type  = is_array($data['mime_type']) ? $data['mime_type'][0] : $data['mime_type'];
        $id_task    = $data['id_task'] ? $data['id_task'] : 0;
        $id_project = $data['id_project'] ?  $data['id_project'] : 0;
        $name       = isset($data['name']) ? $data['name'] : '';
        $res['img'] = [];
        $valid      = false;

        foreach ($file as $f) {
            if($data['type'] == 'url') {
                $fileExt   = $data['ext'];
                $orig_name = $data['orig_name'];
                $size      = $data['size'];
            } else {
                $fileExt   = strtolower($f->getClientOriginalExtension());
                $orig_name = $f->getClientOriginalName();
                $size      = $f->getSize();
            }

            $filename = str_random(10) . time() . str_random(5);
            $path = public_path() . "/images/files/all/";
            $f->move($path, $f->getClientOriginalName());
            rename($path . $f->getClientOriginalName(), $path . $filename .'.'. $fileExt);

            $req['type']       = $type;
            $req['name']       = $name;
            $req['mime_type']  = $mime_type;
            $req['id_task']    = $id_task;
            $req['id_project'] = $id_project;
            $req['orig_name']  = $orig_name;
            $req['size']       = $size;
            $req['file']       = $filename.'.'.$fileExt;
            $req['created_at'] = Carbon::now();

            if(empty($file)) $req['main'] = 1; else $req['main'] = 0;

            $req['id']    = $Mod->t('files')->insertGetId($req);
            $res['img'][] = $req;
        }

        $res['valid'] = $valid;

        return $res;
    }

    /**
     * function for deleting row in table and files linked this table
     */
    public function row_delete()
    {
        $request = $this->requests;
        $Mod = $this->dynamic;

        if(isset($request['table']) || isset($request['id']))
        {
            if($request['table'] != 'files')
            {
                $data['request'] = $Mod->t($request['table'])->where(['id' => $request['id']])->delete();

                $where_table['id_album'] = $request['id'];
                $where_table['name_table'] = $request['table'];
            } else {
                $where_table['id'] = $request['id'];
            }

            $data['album'] = $Mod->t('files')
                ->where($where_table)
                ->get();

            foreach ($data['album'] as $a)
            {
                if($a->file)
                {
                    if(file_exists(public_path() . "/images/files/all/" . $a->file))
                    {
                        unlink(public_path() . "/images/files/all/" . $a->file);
                    }

                    if(file_exists(public_path() . "/images/files/original/" . $a->file))
                    {
                        unlink(public_path() . "/images/files/original/" . $a->file);
                    }

                    if(file_exists(public_path() . "/images/files/big/" . $a->file))
                    {
                        unlink(public_path() . "/images/files/big/" . $a->file);
                    }

                    if(file_exists(public_path() . "/images/files/small/" . $a->file))
                    {
                        unlink(public_path() . "/images/files/small/" . $a->file);
                    }

                    if($a->crop)
                    {
                        if(file_exists(public_path() . "/images/files/original/" . $a->crop))
                        {
                            unlink(public_path() . "/images/files/original/" . $a->crop);
                        }

                        if(file_exists(public_path() . "/images/files/big/" . $a->crop))
                        {
                            unlink(public_path() . "/images/files/big/" . $a->crop);
                        }

                        if(file_exists(public_path() . "/images/files/small/" . $a->crop))
                        {
                            unlink(public_path() . "/images/files/small/" . $a->crop);
                        }
                    }
                }
            }

            $data['album']->toArray();

            $data['request_file'] = $Mod->t('files')->where($where_table)->delete();

            $data['result'] = 'ok';
            $data['mess'] = '';
        } else {
            $data['result'] = 'error';
            $data['mess'] = 'Неверные параметры';
        }

        echo json_encode($data);
    }

    public function get_file($id = null)
    {
        if($id)
        {
            $file        = $this->dynamic->t('files')->where('id', $id)->first();
            $backup_path = $file->type === 'file' ?  public_path() . '/images/files/all/' :  public_path() . '/images/files/original/';
            $file_name   = $file['file'];
            $orig_name   = $file['orig_name'];
            $mime_type   = $file['mime_type'];

            header('Content-Description: File Transfer');
            header("Content-Type: $mime_type");
            header('Content-Disposition: attachment; filename=' . basename($backup_path . $orig_name));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($backup_path . $file_name));

            readfile($backup_path . $file_name);
        }

        exit;
    }
}
