<?php
namespace App\Http\Controllers;

use App\User;
use Auth;
use Hash;
use Request;

class LoginController extends Controller
{
    // соль для пароля
    const SALT = 'rjdney';

    /**
     * страница авторизации
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if(Auth::user())
            return redirect()->intended("/");

        return view('login/index');
    }

    /**
     * авторизация
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function login()
    {
        try {
            $email = Request::input("login");
            $password = Request::input("password");
            $err = "";
            if ($email == "") $err .= " Логин;";
            if ($password == "") $err .= " Пароль;";

            if ($err != "") {
                if (!Request::ajax()) return view("login/index", ["error" => "Не заполнены следующие поля: " . $err]);
                else return response("Не заполнены следующие поля: " . $err, 400);
            }

            $User = User::where("email", $email)->first();

            if ($User == null) {
                if (!Request::ajax()) return view("login/index", ["error" => "Неверный логин"]);
                else return response("Неверный логин", 400);
            }

            $auth = auth()->guard('web');

            $credentials = [
                'email' => $email,
                'password' => $password . self::SALT,
            ];

            if ($auth->attempt($credentials)) return redirect()->intended("/");
            if (!Request::ajax()) return view("login/index", ["error" => "Неверный пароль!"]);

            return response("Неверный пароль!", 400);
        } catch (\Exception $err) {
            return response([$err->getMessage(), $err->getTrace()], 500);
        }
    }

    /**
     * выход
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

}
