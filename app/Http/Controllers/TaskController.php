<?php
namespace App\Http\Controllers;

use App\Classes\Base;
use App\Classes\DynamicModel;
use App\Comment;
use App\Project;
use App\Task;
use App\User;
use App\Userlist;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Session;
use Storage;

class TaskController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct();
        $this->request = $request->all();
        $this->requests = $request;
        $this->dynamic = new DynamicModel();
    }

    /**
     * all tasks
     * @param null $id_project
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index($id_project = null)
    {
        try {
            $tasks                  = new Task();
            $data['user_role']      = Base::$user->role;
            $data['user_role_name'] = Base::$projects_roles[$data['user_role']];
            $data['user_role_text'] = Base::$action_projects_roles[$data['user_role']];
            $projects_access        = Base::projectsAccessByUserId();
            $id_projects            = $projects_access['id'];
            $data['projects']       = $projects_access['projects'];

            $data['id_project'] = $id_project;
            $id_project = $id_project ? ['tasks.projectId' => $id_project] : [];

            $data['tasks_open'] = $tasks
                ->where($id_project)

                ->join('users', function($join)
                {
                    $join->type = 'LEFT OUTER';
                    $join->on('tasks.userAssigned', '=', 'users.id');
                })

                ->join('projects', function($join) use ($id_projects)
                {
                    $join->type = 'RIGHT OUTER';

                    $join
                        ->on('tasks.projectId', '=', 'projects.id')
                        ->whereIn('projects.id', $id_projects);
                })

                ->where('tasks.processes', '!=', 'closed')
                ->select('users.name as user_name', 'tasks.*', 'projects.name as projects_name')
                ->groupBy('tasks.id')
                ->orderBy('tasks.created_at', 'desc')
                ->limit(200)
                ->get();

            $data['tasks_active'] = $tasks
                ->where($id_project)

                ->join('users', function($join)
                {
                    $join->type = 'RIGHT OUTER';
                    $join->on('tasks.userAssigned', '=', 'users.id');
                })

                ->join('projects', function($join) use ($id_projects)
                {
                    $join->type = 'RIGHT OUTER';

                    $join
                        ->on('tasks.projectId', '=', 'projects.id')
                        ->whereIn('projects.id', $id_projects);
                })

                ->whereIN('tasks.processes', Base::$statuses['active'])
                ->select('users.name as user_name', 'tasks.*', 'projects.name as projects_name')
                ->groupBy('tasks.id')
                ->orderBy('tasks.created_at', 'desc')
                ->limit(200)
                ->get();

            $data['tasks_closed'] = $tasks
                ->where($id_project)

                ->join('users', function($join)
                {
                    $join->type = 'LEFT OUTER';
                    $join->on('tasks.userAssigned', '=', 'users.id');
                })

                ->join('projects', function($join) use ($id_projects)
                {
                    $join->type = 'RIGHT OUTER';

                    $join
                        ->on('tasks.projectId', '=', 'projects.id')
                        ->whereIn('projects.id', $id_projects);
                })

                ->where('tasks.processes', '=', 'closed')
                ->select('users.name as user_name', 'tasks.*', 'projects.name as projects_name')
                ->groupBy('tasks.id')
                ->orderBy('tasks.created_at', 'desc')
                ->limit(200)
                ->get();

            $data['statuses_text']  = Base::$statuses_text;
            $data['statuses_color'] = Base::$statuses_color;

            return Base::view('task.index', $data);
        } catch (\Exception $err) {
            return Base::view('errors.error', ['debug' => false]);
        }
    }

    /**
     * страница добавления задачи
     * @param $id - id проекта
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function add($id)
    {
        try {
            $Mod = $this->dynamic;
            $data['id'] = $id;
            $data['request'] = $this->request;
            $data['old'] = $this->requests->old();

            if ((int)$id < 1) return Base::wrong('Не правильный id!');
            $data['project'] = Project::find($id);

            if ($data['project'] == null) return Base::wrong('Нет такого проекта!');
            $userlist = $Mod->t('userlist')->select('userId')->where('projectId', $data['project']->id)->get()->toArray();

            $user_list = [];
            foreach ($userlist as $v) {
                $user_list[] = $v['userId'];
            }

            $data['project_users'] = $Mod->t('users')
                ->whereIn('id', $user_list)
                ->get();

            return Base::view('task.add', $data);
        } catch (\Exception $err) {
            return Base::view('errors/error', ['debug' => false]);
        }
    }

    /**
     * добавление задачи
     * @param $id - id проекта
     * @param $apply - save and open edit
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function add_post($id, $apply = null)
    {
        try {
            $project = Project::find($id);

            if ((int)$id < 1) return Base::wrong('Не правильный id!');
            if (empty($project)) return Base::wrong('Нет такого проекта!');
            if (empty($this->request['name'])) return Base::wrong('Нет имени у задачи!');
            if (empty($this->request['description'])) return Base::wrong('Нет описания у задачи!');

            $task = new Task();

            $task->name        = $this->request['name'];
            $task->description = $this->request['description'];
            $task->progress    = 0;
            $task->status      = 'new';
            $task->priority    = $this->request['priority'];
            $task->number_task = ++$project->current_task;
            $deadline          = explode('/', $this->request['deadline']);

            if(isset($deadline[1]))
                $task->deadline = $deadline[2] .'-'. $deadline[1] .'-'. $deadline[0];

            $i = 0;
            $Qtasks = [];
            $tasks_in_task = isset($this->request['tasks']) ? $this->request['tasks'] : [];
            foreach ($tasks_in_task as $b) {
                $Qtasks[$i]['text'] = $b['text'];
                $Qtasks[$i]['status'] = $b['status'];

                $i++;
            }

            $task->tasks        = json_encode($Qtasks);
            $task->projectId    = $id;
            $task->userCreate   = Auth::user()->id;
            $task->userAssigned = $this->request['userAssigned'];
            $task->created_at   = Carbon::now();
            $task->updated_at   = Carbon::now();

            $task->save();

            $project->current_task = $task->number_task;

            $project->save();

            $users = isset($this->request['user']) ? $this->request['user'] : [];

            foreach ($users as $i => $_id) {
                $user_list = new Userlist();

                $user_list->userId = $_id;
                $user_list->role   = 3;
                $user_list->taskId = $task->id;

                $user_list->save();
            }

            Base::activity("добавил задчу <a href='/task/detailed/$task->id'>$task->name</a>", 'create', $id);

            if($apply) {
                return redirect("/task/edit/$task->id");
            } else {
                return Base::redirect("/project/$id", "Задача $task->name успешно добавлена");
            }
        } catch (\Exception $err) {
            return Base::view('errors/error', ['debug' => false]);
        }
    }

    /**
     * редактирование задачи
     * @param $id - id задачи
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        try {
            $data['id']        = $id;
            $data['task']      = Task::find($id);
            $data['project']   = Project::find($data['task']->projectId);
            $userlist          = Userlist::where(['taskId' => $data['task']->id])->get();
            $data['allUsers']  = User::all();
            $data['user_list'] = [];

            for ($i = 0; count($userlist) > $i; $i++) {
                $data['user_list'][$userlist[$i]->userId] = $userlist[$i];
            }

            $deadline = explode('-', $data['task']->deadline);

            if(isset($deadline[1]))
                $data['task']->deadline = explode(' ', $deadline[2])[0] .'/'. $deadline[1] .'/'. $deadline[0];

            return Base::view('task.edit', $data);
        } catch (\Exception $err) {
            return Base::view('errors/error', ['debug' => false]);
        }
    }

    /**
     * редактирование задачи
     * @param $id - id задачи
     * @param $apply - save and open edit
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit_save($id, $apply = null)
    {
        try {
            $task = Task::find($id);

            if ((int)$id < 1) return Base::wrong('Не правильный id!');
            if (!isset($task->id)) return Base::wrong('Нет такой задачи!');
            if (empty($this->request['name'])) return Base::wrong('Нет имени у задачи!');
            if (empty($this->request['description'])) return Base::wrong('Нет описания у задачи!');

            $task->name        = $this->request['name'];
            $task->description = $this->request['description'];
            $task->progress    = 0;

            if ($task->processes == 'closed') {
                $task->status = 'reopen';
            } else {
                if ($task->status != 'new') $task->status = 'update';
            }

            $task->priority = $this->request['priority'];
            $deadline = explode('/', $this->request['deadline']);

            if(isset($deadline[1])) {
                $task->deadline = $deadline[2] .'-'. $deadline[1] .'-'. $deadline[0];
            }

            $i = 0;
            $Qtasks = [];
            $tasks_in_task = isset($this->request['tasks']) ? $this->request['tasks'] : [];
            foreach ($tasks_in_task as $b) {
                $Qtasks[$i]['text']   = $b['text'];
                $Qtasks[$i]['status'] = $b['status'];

                $i++;
            }

            $task->tasks        = json_encode($Qtasks);
            $task->userAssigned = $this->request['userAssigned'];
            $task->attachment   = $this->request['attachment'];
            $task->updated_at   = Carbon::now();
            $id_project         = $task->projectId;

            $task->save();

            $users = isset($this->request['user']) ? $this->request['user'] : [];

            Userlist::where(['taskId' => $task->id, 'projectId' => null])->delete();

            foreach ($users as $i => $_id) {
                $user_list = new Userlist();

                $user_list->userId = $_id;
                $user_list->role   = 3;
                $user_list->taskId = $task->id;

                $user_list->save();
            }

            Base::activity("Отредактировал задчу <a href='/task/detailed/$task->id'>$task->name</a>", 'edit', $id_project);

            if($apply) {
                return Base::redirect("/task/edit/" . $task->id, "Задача $task->name успешно отредактирована");
            } else {
                return Base::redirect("/task/detailed/" . $task->id, "Задача $task->name успешно отредактирована");
            }
        } catch (\Exception $err) {
            return Base::view('errors/error', ['debug' => false]);
        }
    }

    /**
     * вывод детального вида задачи
     * @param $id - id задачи
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function detailed($id)
    {
        try {
            if ((int)$id < 1) return Base::wrong('Не верный id!');
            $data['task'] = Task::find($id);

            if ($data['task'] == null) return Base::wrong('Нет такой задачи!');
            $data['project'] = Project::find($data['task']->projectId);

            if (!$data['project']) return Base::wrong('У этой задачи нет проекта!!!');
            $userList = Userlist::where('projectId', $data['project']->id)->where('userId', Base::$user->id)->first();

            if (!$userList && $data['project']->userId != Base::$user->id) return Base::wrong("Вы не добавлены в проект!");
            $projectRole = $userList->role;
            $userList = Userlist::where('taskId', $data['task']->id)->where('userId', Base::$user->id)->first();

            if (!$userList && $data['task']->userCreate != Base::$user->id
                && $data['task']->userAssigned != Base::$user->id && Base::project_role(['A', 'M'], $projectRole)
            ) return Base::wrong("Вы не добавлены в задачу!");

            $data['user_create']   = User::find($data['task']->userCreate);
            $data['user_assigned'] = User::find($data['task']->userAssigned);
            $data['user_list']     = Userlist::where('taskId', $id)->get();
            $data['all_users']     = User::all();
            $data['users']         = [];

            foreach ($data['all_users'] as $u) $data['users'][$u->id] = $u;

            $data['comments']   = Comment::where('taskId', $id)->get();
            $data['attachment'] = json_decode($data['task']->attachment, true);

            if (!$data['attachment']) $data['attachment'] = [];

            if (!$data['task']->read && $data['task']->userAssigned == Base::$user->id) {
                $data['task']->read = true;
                $id_project         = $data['task']->projectId;

                $data['task']->save();

                Base::activity(
                    "посмотрел задачу <a href='/task/detailed/" . $data['task']->id . ">" . $data['task']->name . "</a>",
                    'edit',
                    $id_project
                );
            }

            if ($data['task']->status != 'reading' && $data['task']->userAssigned == Base::$user->id) {
                if ($data['task']->status != 'reading') $data['task']->status = 'reading';

                $data['task']->save();
            }

            $data['statuses']       = Base::$statuses;
            $data['task_status']    = Base::$task_status;
            $data['statuses_text']  = Base::$statuses_text;
            $data['statuses_color'] = Base::$statuses_color;

            return Base::view('task.detailed', $data);
        } catch (\Exception $err) {
            return Base::view('errors.error', ['debug' => false]);
        }
    }

    /**
     * переназначение задачи
     * $userId - id пользователя, которому назначаем
     * $taskId - id задачи
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function reassigned()
    {
        try {
            $userId = !empty($this->request['reassignedUser']) ? $this->request['reassignedUser'] : 0;
            $taskId = !empty($this->request['taskId']) ? $this->request['taskId'] : 0;

            if ((int)$userId < 1 || (int)$taskId < 1) return Base::wrong('Не правильный id!');
            $Task = Task::find($taskId);

            if ($Task == null) return Base::wrong('Нет такой задачи!');
            $User = User::find($userId);

            if ($User == null) return Base::wrong('Нет такого пользователя!');
            $Task->userAssigned = $userId;
            $id_project         = $Task->projectId;

            $Task->save();

            Base::activity(
                "нзаначил задачу <a href='/task/detailed/$Task->id'>$Task->name</a> пользователю <a href='#'>$User->name</a>",
                'edit',
                $id_project
            );

            return Base::back("Задача $Task->name назначена пользователю $User->name");
        } catch (\Exception $err) {
            return Base::view('errors/error', ['debug' => false]);
        }
    }

    /**
     * удаление задачи
     * $taskId - id задачи
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function delete()
    {
        try {
            $taskId = !empty($this->request['taskId']) ? $this->request['taskId'] : 0;

            if ((int)$taskId < 1) return Base::wrong('Не правильный id!');
            $Task = Task::find($taskId);

            if ($Task == null) return Base::wrong('Нет такой задачи!');
            $attachments = json_decode($Task->attachment, true);

            if (!empty($attachments)) {
                foreach ($attachments as $file) {
                    unlink(base_path() . Base::UPLOAD_PATH . $file['serverName']);
                }
            }

            Comment::where('taskId', $taskId)->delete();
            Userlist::where('taskId', $taskId)->delete();
            $name = $Task->name;
            $id_project = $Task->projectId;

            $Task->delete();

            Base::activity("удалил задачу $name", 'delete', $id_project);

            return Base::back("Задача $name и связанные с ней файлы удалены");
        } catch (\Exception $err) {
            return Base::view('errors/error', ['debug' => false]);
        }
    }

    public function close()
    {
        try {
            $taskId = !empty($this->request['taskId']) ? $this->request['taskId'] : 0;

            if (empty($taskId)) return Base::wrong('Нет id!');
            if ((int) $taskId < 1) return Base::wrong('Не правиьный id!');
            $task = Task::find($taskId);

            if ($task == null) return Base::wrong('Нет такой задачи!');
            $task->processes = 'closed';

            $task->save();

            return Base::back('Задача успешно закрыта!');
        } catch (\Exception $err) {
            return Base::view('errors/error', ['debug' => false]);
        }
    }

}