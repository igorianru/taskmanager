<?php
namespace App\Http\Controllers;

use App\Project;
use App\Userlist;
use Illuminate\Http\Request;
use Auth;
use App\Classes\Base;
use Response;
use DB;
use App\Activity;
use App\Classes\DynamicModel;

class MainController extends Controller
{

    public function __construct(Request $request)
    {
        parent::__construct();
        $this->request = $request->all();
        $this->requests = $request;
        $this->dynamic = new DynamicModel();
    }

    /**
     * рабочий стол, вывод всех доступных пользователю проектов
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        try {
            $Mod = $this->dynamic;
            $data['user_role'] = Base::$user->role;
            $data['user_role_name'] = Base::$projects_roles[$data['user_role']];
            $data['user_role_text'] = Base::$action_projects_roles[$data['user_role']];

            if($data['user_role'] == 'SA') {
                $data['projects'] = $Mod->t('projects')->get();
            } else {
                $data['projects'] = $Mod
                    ->t('projects')

                    ->join('userlist', function($join)
                    {
                        $join->type = 'INNER';
                        $join->on('projects.id', '=', 'userlist.projectId');
                    })

                    ->where('userlist.userId', '=', Base::$user->id)
                    ->select('projects.*', 'userlist.role')
                    ->groupBy('projects.id')
                    ->orderBy('projects.created_at', 'desc')
                    ->get();
            }

            $userlist = $Mod->t('userlist')->where(['userId' => Base::$user->id])->groupBy('projectId')->get();

            $right_project_id = [];
            foreach ($userlist as $v) {
                $right_project_id[] = $v->projectId;
            }

            $data['activities'] = $Mod->t('activity')

                ->join('users', function($join)
                {
                    $join->type = 'LEFT OUTER';
                    $join->on('activity.userId', '=', 'users.id');
                })

                ->whereIn('activity.id_project', $right_project_id)

                ->select('users.*', 'activity.text', 'activity.created_at', 'activity.action')
                ->groupBy('activity.id')
                ->orderBy('activity.created_at', 'ASC')
                ->paginate(10);

            $data['tasks_open'] = $Mod->t('tasks')
                ->join('users', function($join)
                {
                    $join->type = 'LEFT OUTER';
                    $join->on('tasks.userAssigned', '=', 'users.id');
                })

                ->join('projects', function($join)
                {
                    $join->type = 'LEFT OUTER';
                    $join->on('tasks.projectId', '=', 'projects.id');
                })

                ->where('projects.status', '!=', 'closed')
                ->where('tasks.processes', '!=', 'closed')
                ->whereIn('tasks.projectId', $right_project_id)
                ->select('users.name as user_name', 'tasks.*', 'projects.name as projects_name')
                ->groupBy('tasks.id')
                ->orderBy('tasks.id', 'DESC')
                ->limit(200)
                ->get();

            $data['statuses_text'] = Base::$statuses_text;
            $data['statuses_color'] = Base::$statuses_color;
            $data['pagination'] = ''.$data['activities']->render();

            return Base::view('dashboard.index', $data);
        } catch (\Exception $err){
            return Base::view('errors.error', ['debug'=>false]);
        }
    }

    /**
     * загрузка файлов с сервера на клиент
     * @param $realName - имя файла
     * @param $serverName - серверное имя файла
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|Response
     */
    public function download($realName,$serverName)
    {
        try {
            return Response::download(base_path().Base::UPLOAD_PATH . $serverName,$realName);
        } catch (\Exception $err){
            return response([$err->getMessage(),$err->getTrace()]);
            return Base::view('errors/error', ['debug' => false]);
        }
    }

    public function saveIp()
    {
        $file='getIp.log';
        error_log("---Begin record---\n",3,$file);
        error_log('Date: '.date('Y-m-d H:m:s',time())."\n",3,$file);
        error_log('$_SERVER array:'."\n",3,$file);
        foreach($_SERVER as $key=>$value){
            error_log("\t".$key.": ".$value."\n",3,$file);
        }
        error_log("---End record---\n\n",3,$file);
        echo "<h3>Если в течении 10 секунд вы не видите видео, <a href='#'>нажмите сюда</a> </h3>";
        return;
    }

    public function filterCSV()
    {
        \App::setLocale('ru');
        $fp = fopen('file1.csv', 'w');
        $date2=strtotime('2016-02-04 00:02:50');
        if (($handle = fopen("phones_all.csv", "r")) !== FALSE) {
//            $data=fgetcsv($handle, 0, ";");
//            $data=fgetcsv($handle, 0, ";");
//            echo iconv('windows-1251','utf-8',$data[11]);
//            dump($data);
//            if (strpos(iconv('windows-1251','utf-8',$data[11]),' Волгоградская обл.')!==false) echo 'ok';
            while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
                $date1=strtotime($data[1]);
//                if (!empty($data[3]) &&
//                    strpos(iconv('windows-1251','utf-8',$data[11]),'Волгоградская обл.')!==false &&
//                    strpos(iconv('windows-1251','utf-8',$data[11]),'Волжский г.')!==false)
                foreach($data as $d) {
                    if (strpos($d,'141.0.13.174')!==false)
                        fputcsv($fp, $data);
                }
            }
            fclose($handle);
        }
        fclose($fp);
        return "Done";
    }

    public function test()
    {
        if (empty($_REQUEST)) {
            $param = explode('?', $_SERVER['REQUEST_URI']);
            parse_str($param[1], $request);
        } else $request=$_REQUEST;
        $answer='unknown';
        if (isset($request['test'])){
            $n=$request['test'];
            if ($n=='1'){
                $answer=array(
                    "response"=>"вы оплатили",
                    'error'=>0
                );
            } else {
                $answer=array(
                    'status'=>'ok'
                );
            }
        }
        return json_encode($answer);
    }

    /**
     * function to actions gui
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function guiSend() {
//        try {
            $Mod = $this->dynamic;
            $request = $this->request;
            $req['result'] = 'error';

            if(isset($request['actions'])) {
                switch ($request['actions']) {
                    case 'delete_project':
                        $project = new ProjectController($this->requests);
                        $project->delete($request['id']);
                        $req['result'] = 'ok';

                        break;
                    case 'delete_task':
                        $Mod->t($request['table'])->where(['id' => $request['id']])->delete();
                        $req['result'] = 'ok';

                        break;
                    case 'close_task':
                        $task = $Mod->t($request['table'])->where(['id' => $request['id']])->first();
                        $task->processes = 'closed';

                        $task->save();
                        $req['result'] = 'ok';

                        break;
                    case 'status_project':
                        $project = $Mod->t($request['table'])->where(['id' => $request['id']])->first();

                        if($project->ststus == 'closed') {
                            $project->status = 'update';
                        } else {
                            $project->status = 'closed';
                        }

                        $project->save();
                        $req['result'] = 'ok';
                        $req['param'] = $project->status;

                        break;
                    case 'processes_project':
                        $task = $Mod->t($request['table'])->where(['id' => $request['id']])->first();

                        $task->processes = $request['param'];

                        $task->save();
                        $req['result'] = 'ok';
                        $req['param'] = $task->processes;

                        break;
                    case 'checked_task':
                        $task = $Mod->t($request['table'])->where(['id' => $request['id']])->first();

                        $i = 0;
                        $Qtasks = [];
                        $tasks_in_task = json_decode($task->tasks, true);

                        foreach ($tasks_in_task as $b) {
                            if($i == $request['ids']) {
                                $Qtasks[$i]['status'] = $request['param'];
                            } else {
                                $Qtasks[$i]['status'] = $b['status'];
                            }

                            $Qtasks[$i]['text'] = $b['text'];
                            $i++;
                        }

                        $task->tasks = json_encode($Qtasks);

                        $task->save();
                        $req['result'] = 'ok';
                        $req['param'] = $request['param'];

                        break;
                }
            }

            echo json_encode($req);
//        } catch (\Exception $err){
//            return Base::view('errors.error', ['debug' => false]);
//        }
    }
}