<?php
namespace App\Http\Controllers;

use App\Classes\Base;
use App\Comment;
use App\Project;
use App\Task;
use App\User;
use App\Userlist;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ProjectController extends Controller
{

    public function __construct(Request $request)
    {
        parent::__construct();
        $this->request = $request->all();
        $this->requests = $request;
    }

    /**
     * страница добавления проекта
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        try {
            $data['projects'] = DB::select("select * from projects
                                      where userId=" . Base::$user->id . "
                                      or id in (
                                        select projectId from userlist
                                        where projectId is not null
                                        and userId=" . Base::$user->id . "
                                      )");

            return Base::view('project.index', $data);
        } catch (\Exception $err) {
            return Base::view('errors.error', ['debug' => true, 'errMessage' => $err->getMessage()]);
        }
    }

    /**
     * добавление проетка
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function add()
    {
        try {
            $request = $this->request;

            if (isset($request['name'])) {
                $project = new Project();

                $project->name = $request['name'];
                $project->description = $request['description'];
                $project->status = 'new';
                $project->userId = Base::$user->id;
                $project->created_at = Carbon::now();
                $project->updated_at = Carbon::now();

                $project->save();

                $Users = $this->request['user'];

                foreach ($Users as $i => $id) {
                    $Userlist = new Userlist();

                    $Userlist->userId = $id;
                    $Userlist->role = $request['role'][$i];
                    $Userlist->projectId = $project->id;

                    $Userlist->save();
                }

                Base::activity(
                    "добавил проект <a href='/project/$project->id'>$project->name</a>",
                    'create',
                    $project->id
                );

                return Base::redirect("/", "Проект $project->name успешно добавлен");
            } else {
                $data['allUsers'] = User::all();

                return Base::view('project.add_project', $data);
            }
        } catch (\Exception $err) {
            return Base::view('errors.error', ['debug' => false]);
        }
    }

    /**
     * редактирование проетка
     * * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        try {
            if(!Base::project_role(['A', 'M'], $id)) return Base::wrong('Нет прав для редактирования.');

            $data['project'] = Project::where(['id' => $id])->first();
            $data['user_list'] = Userlist::where(['projectId' => $id])->get();
            $data['allUsers'] = User::all();

            return Base::view('project.edit', $data);
        } catch (\Exception $err) {
            return Base::view('errors.error', ['debug' => true, 'errMessage' => $err->getMessage()]);
        }
    }

    /**
     * редактирование проетка
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit_save($id)
    {
        try {
            $project = Project::find($id);

            if ((int)$id < 1) return Base::wrong('Не правильный id!');
            if (!isset($project->id)) return Base::wrong('Нет такого проекта');
            if (empty($this->request['name'])) return Base::wrong('Нет имени у проекта!');
            if (empty($this->request['description'])) return Base::wrong('Нет описания у проекта!');

            $project->name = $this->request['name'];
            $project->description = $this->request['description'];
            $project->status = 'update';
            $project->updated_at = Carbon::now();

            $project->save();

            Userlist::where(['taskId' => null, 'projectId' => $project->id])->delete();

            foreach ($this->request['user'] as $k => $id) {
                $userlist = new Userlist();

                $userlist->userId = $id;
                $userlist->role = $this->request['role'][$k];
                $userlist->projectId = $project->id;

                $userlist->save();
            }

            Base::activity(
                "добавил проект <a href='/project/$project->id'>$project->name</a>",
                'edit',
                $project->id
            );

            return Base::redirect("/", "Проект $project->name успешно отредактирован");
        } catch (\Exception $err){
            return Base::view('errors.error', ['debug'=>false]);
        }
    }

    /**
     * страница просмотра задачь проекта
     * @param $id - id проекта
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function view($id)
    {
        try {
            if(!Base::project_role(['A', 'M'], $id)) return Base::wrong("Нет прав на просмотр данного проекта");

            $data['user_role']      = Base::$user->role;
            $data['user_role_name'] = Base::$projects_roles[$data['user_role']];
            $data['user_role_text'] = Base::$action_projects_roles[$data['user_role']];

            $tasks = new Task();

            if ((int)$id < 1) return Base::wrong("Не правильный id!");
            $data['project'] = Project::find($id);

            if ($data['project'] == null) return Base::wrong("Нет такого проекта!");

            $data['tasks_open'] = $tasks
                ->where(['tasks.projectId' => $id])
                ->join('users', function($join)
                {
                    $join->type = 'LEFT OUTER';
                    $join->on('tasks.userAssigned', '=', 'users.id');
                })

                ->where('tasks.processes', '!=', 'closed')
                ->select('users.name as user_name', 'tasks.*')
                ->groupBy('tasks.id')
                ->orderBy('tasks.created_at', 'desc')
                ->limit(200)
                ->get();

            $data['tasks_active'] = $tasks
                ->where(['tasks.projectId' => $id])
                ->join('users', function($join)
                {
                    $join->type = 'LEFT OUTER';
                    $join->on('tasks.userAssigned', '=', 'users.id');
                })

                ->whereIN('tasks.processes', Base::$statuses['active'])
                ->select('users.name as user_name', 'tasks.*')
                ->groupBy('tasks.id')
                ->orderBy('tasks.created_at', 'desc')
                ->limit(200)
                ->get();

            $data['tasks_closed'] = $tasks
                ->where(['tasks.projectId' => $id])
                ->join('users', function($join)
                {
                    $join->type = 'LEFT OUTER';
                    $join->on('tasks.userAssigned', '=', 'users.id');
                })

                ->where('tasks.processes', '=', 'closed')
                ->select('users.name as user_name', 'tasks.*')
                ->groupBy('tasks.id')
                ->orderBy('tasks.created_at', 'desc')
                ->limit(200)
                ->get();

            if ($data['user_role'] == 'A' || $data['user_role'] == 'SA') {
                $data['project_role'] = 1;
            } else {
                $data['project_role'] = Userlist::where('projectId', $data['project']->id)
                    ->where('userId', Base::$user->id)
                    ->first();

                if ($data['project_role'] == null) return Base::wrong("Доступ запрещён");
                $data['project_role'] = $data['project_role']->role;
            }

            $data['users'] = Userlist::where('projectId', $id)
                ->join('users', function($join)
                {
                    $join->type = 'LEFT OUTER';
                    $join->on('userlist.userId', '=', 'users.id');
                })

                ->groupBy('userlist.userId')
                ->orderBy('users.id', 'desc')
                ->paginate(10);

            $data['users_count']    = count($data['users']);
            $data['pagination']     = ''.$data['users']->render();
            $data['statuses_text']  = Base::$statuses_text;
            $data['statuses_color'] = Base::$statuses_color;

            return Base::view('project.project_id', $data);
        } catch (\Exception $err) {
            return Base::view('errors.error', ['debug' => false]);
        }
    }

    /**
     * удаление проекта
     * @param $id - id проекта
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        try {
            if ((int)$id < 1) return Base::wrong('Не правильный id!');
            $project = Project::find($id);

            if ($project == null) return Base::wrong('Нет такого проекта!');
            if (Base::$user->role != 'A') return Base::wrong('Нет прав для удаления!');

            $name = $project->name;
            $tasks = Task::where('projectId', $id)->get();

            foreach ($tasks as $val) {
                $attachments = unserialize($val->attachment);

                if (!empty($attachments)) {
                    foreach ($attachments as $file) {
                        unlink(base_path() . Base::UPLOAD_PATH . $file['serverName']);
                    }
                }

                Comment::where('taskId', $val->id)->delete();
                Userlist::where('taskId', $val->id)->delete();
            }

            $tasks = Task::where('projectId', $id)->delete();
            Userlist::where('projectId', $project->id)->delete();

            $name = $project->name;
            $project_id = $project->id;

            $project->delete();

            if($tasks) {
                Base::activity("удалил проект $name", 'delete', $project_id);
            } else {
                Base::activity("совершил попытку удалить проект. ошибка удаления, для $name", 'delete', $project_id);
            }
        } catch (\Exception $err) {
            return Base::view('errors.error', ['debug' => false]);
        }
    }

}
