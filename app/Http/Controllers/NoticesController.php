<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use \App\Classes\Base;

class NoticesController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct();
        $this->request = $request->all();
    }


    public function send_alert_comm($data = array())
    {
        $data['id_user'] = Auth::user()->id;
        $data['column'] = 'comment';

        $res = $this->_check_alert_time($data);

        var_dump($res);
    }

    public function send_alert_mess($data = array())
    {
        try {
            $request = $this->request;

            if(isset($request['id_user']))
            {
                $data['id_user'] = $request['id_user'];
                $data['column'] = 'message';
                $data['body'] = $request;

                $res = $this->_check_alert_time($data);

                $user = User::findOrFail($data['id_user']);
                $data['alert'] = $user->text_alert_message;

                if($res === false)
                {
                    $res = $this->_send_mess_to_mail($data);

                    $user->text_alert_message = 0;
                    $user->alert_message = new \DateTime();
                } else {
                    $user->text_alert_message = $user->text_alert_message + 1;
                }

                $user->save();
            } else {
                $res['result'] = false;
            }

            return $res['result'];
        } catch (\Exception $err) {
            return Base::view('errors/error', ['debug' => false]);
        }
    }
// проверка разницы во времени относительно разрешенного лимита отправки оповещений
    private function _check_alert_time($data)
    {
        try {
            if(!isset($data['id_user']))
            {
                $data['id_user'] = Auth::user()->id;
            }

            $user = User::findOrFail($data['id_user']);

            $column_al = 'alert_' . $data['column'];
            $old_lime = $user->$column_al;
            $column_tm = 'time_alert_' . $data['column'];
            $fix_alert = $user->$column_tm;

            if($fix_alert == -1)
            {
                return false;
            }

            if($fix_alert)
            {
                $fix_alert = 3600;
            }

            $d1 = new \DateTime($old_lime);
            $d2 = new \DateTime();

            $d = $d1->diff($d2)->days * (60 * 60 * 24);
            $h = $d1->diff($d2)->h * (60 * 60);
            $i = $d1->diff($d2)->i * 60;
            $s = $d1->diff($d2)->s;

            $old_lime = ($d + ($h - ($i + $s)));

            if(($old_lime - $fix_alert) >= $fix_alert) {
                $res = true;
            } else {
                $res = false;
            }

            return $res;
        } catch (\Exception $err) {
            return Base::view('errors/error', ['debug' => false]);
        }
    }

// отправка уведомлений на почту
    private function _send_mess_to_mail($data)
    {
        try {
            if(!isset($data['id_user']))$data['id_user'] = Auth::user()->id;
            if(isset($data['alert'])) $data['body']['alert'] = $data['alert'];

            $user = User::findOrFail($data['id_user']);
            if(isset($data['body']['id_user_from']))
            {
                $user_from = User::findOrFail($data['body']['id_user_from']);
                $data['body']['user_from'] = $user_from;
            }

            $data['body']['url'] = Base::BASE_URL;

//            $mail = Mail::send('notices.reminder', ['user' => $user, 'body' => $data['body']], function ($m) use ($user) {
//                $m->from('robot@rhenm.ru', 'TaskManager');
//
//                $m->to($user->email, $user->name)->subject('Оповещение');
//            });
            $mail = true;

            $req['result'] = $mail;

            return $req;
        } catch (\Exception $err) {
            return Base::view('errors/error', ['debug' => false]);
        }
    }
}
