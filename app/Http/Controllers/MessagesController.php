<?php
namespace App\Http\Controllers;

use App\Classes\Base;
use App\Messages;
use App\Dialogs;
use App\Task;
use App\Userlist;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Session;

class MessagesController extends Controller
{

    public function __construct(Request $request)
    {
        parent::__construct();
        $this->request = $request->all();
    }

    public function index()
    {
        try {
            $data['all_users'] = User::all();

            return Base::view('messages.dialogs', $data);
        } catch (\Exception $err){
            return Base::view('errors.error', ['debug' => false]);
        }
    }

}
