<?php
Route::group(['middleware' => ['web']], function () {
    // роуты авторизации
    Route::get('/films/my_video.mp4','MainController@saveIp');
    Route::get('/test','MainController@test');
    Route::get('/filtercsv','MainController@filterCSV');
    Route::get('login','LoginController@index');
    Route::get('logout','LoginController@logout');
    Route::post('login','LoginController@login');
});

Route::group(['middleware' => ['web','auth']], function () {
    Route::get('/','UserController@profile_redirect');

    // роуты рабочего стола
    Route::get('/dashboard','MainController@index');

    // роуты проекта
    Route::get('projects','ProjectController@index');
    Route::get('project/{id}','ProjectController@view');
    Route::any('projects/add','ProjectController@add');
    Route::get('/project/edit/{id}','ProjectController@edit');
    Route::post('/project/edit/{id}','ProjectController@edit_save');

    // роуты задач
    Route::get('tasks','TaskController@index');
    Route::get('tasks/{id_project?}','TaskController@index');
    Route::get('task/{id}','TaskController@add');
    Route::get('task/detailed/{id}','TaskController@detailed');
    Route::post('/task/add/{id}/{apply?}','TaskController@add_post');
  //  Route::post('/task/add/{id}/','TaskController@add_post');
    Route::get('task/edit/{id}','TaskController@edit');
    Route::post('task/edit/{id}/{apply?}','TaskController@edit_save');
    Route::post('task/reassigned','TaskController@reassigned');
    Route::post('task/close','TaskController@close');
    Route::post('task/{taskId}/addcomment','CommentController@add');

    Route::post('comment/delete','CommentController@delete');

    Route::get('users','UserController@index');
    Route::get('userAdd','UserController@viewAdd');
    Route::get('user/profile/{id}','UserController@profile_id');

    Route::get('user/profile_edit/{id}','UserController@profile_edit');
    Route::post('user/profile_edit/{id?}','UserController@profile_edit');

    Route::post('user/add','UserController@add');
    Route::post('user/delete','UserController@delete');

    Route::any('/main/gui-send','MainController@guiSend');

    // files
    Route::get('file/download/{realName}/{serverName}','MainController@download');
    Route::post('/files/load_files/','FilesController@load_files');
    Route::post('/files/get_files/','FilesController@get_files');
    // delete row in mySQL
    Route::post('/files/row_delete','FilesController@row_delete');
    Route::get('/files/get_file/{id}','FilesController@get_file');

    // роуты messages
    Route::get('messages','MessagesController@index');
    // роуты notices
    Route::post('/notices/send_alert_mess','NoticesController@send_alert_mess');
    Route::get('/notices/send_alert_comm','NoticesController@send_alert_comm');
});